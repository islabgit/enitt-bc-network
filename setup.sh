#!/bin/bash

# export VERSION=1.4.3
# export CA_VERSION=1.4.3

export PATH=${PWD}/bin/macos:$PATH

# docker configuration
IMAGETAG=1.4.8

CHANNEL_ARTIFACTS_DIR=energy-channel-artifacts
CRYPTO_DIR=crypto-config
COMPOSE_CLI_FILE="docker-compose-cli.yaml"
COMPOSE_LOCAL_FILE="docker-compose-local.yaml"
COMPOSE_COUCH_FILE="docker-compose-couch.yaml"
COMPOSE_CA_FILE="docker-compose-ca.yaml"

SYS_CHANNEL="energy-sys-channel"
CHANNEL_NAME="energy-channel"

CLI_DELAY=1
CLI_TIMEOUT=10

printHelp() {
  echo "Usage: setup.sh [options]"
  echo
  echo "options:"
  echo "-h : this help"
  echo "-c : generate crypto-config and channel artifacts"
  echo "-l : run local networks"
  echo
  echo "e.g. init.sh -c"
  echo "would make docker images and binaries for version 1.4.3"
}

generateCryptoCredentials() {
  echo "generate crypto-config"
  if [ -d $CRYPTO_DIR ]; then
    sudo rm -rf $CRYPTO_DIR/*
  else
    mkdir $CRYPTO_DIR
  fi
  docker run -it --rm -v ${PWD}:/workdir --workdir /workdir hyperledger/fabric-tools:$IMAGETAG cryptogen generate --config=crypto-config.yaml
  sudo chmod 755 ${PWD}/crypto-config -R
  #cryptogen generate --config=./crypto-config.yaml --output=$CRYPTO_DIR
}

generateChannelArtifacts() {
  echo "generate channel artifacts"

  if [ -d $CHANNEL_ARTIFACTS_DIR ]; then
    rm -rf $CHANNEL_ARTIFACTS_DIR/*
    mkdir $CHANNEL_ARTIFACTS_DIR
  else
    mkdir $CHANNEL_ARTIFACTS_DIR
  fi
  echo ${PWD}
  # generate genesis block
  docker run -it --rm -v ${PWD}:/workdir --workdir /workdir hyperledger/fabric-tools:$IMAGETAG configtxgen -configPath /workdir -profile EnergyNetwork -channelID $SYS_CHANNEL -outputBlock $CHANNEL_ARTIFACTS_DIR/genesis.block
  # generate CreateChannelTx
  docker run -it --rm -v ${PWD}:/workdir --workdir /workdir hyperledger/fabric-tools:$IMAGETAG configtxgen -configPath /workdir -profile EnergyChannel -outputCreateChannelTx $CHANNEL_ARTIFACTS_DIR/channel.tx -channelID $CHANNEL_NAME

  echo
  echo "#################################################################"
  echo "#######  Generating anchor peer update for ENITT MSP   ##########"
  echo "#################################################################"
  set -x
  docker run -it --rm -v ${PWD}:/workdir --workdir /workdir hyperledger/fabric-tools:$IMAGETAG configtxgen -configPath /workdir -profile EnergyChannel -outputAnchorPeersUpdate $CHANNEL_ARTIFACTS_DIR/ENITTMSPanchors.tx -channelID $CHANNEL_NAME -asOrg ENITTMSP
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate anchor peer update for ENITT MSP..."
    exit 1
  fi

  echo
  echo "#################################################################"
  echo "#######    Generating anchor peer update for SC  MSP   ##########"
  echo "#################################################################"
  set -x
  docker run -it --rm -v ${PWD}:/workdir --workdir /workdir hyperledger/fabric-tools:$IMAGETAG configtxgen -configPath /workdir -profile EnergyChannel -outputAnchorPeersUpdate $CHANNEL_ARTIFACTS_DIR/SCMSPanchors.tx -channelID $CHANNEL_NAME -asOrg SCMSP
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate anchor peer update for SC MSP..."
    exit 1
  fi
  echo


  echo
  echo "#################################################################"
  echo "#######    Generating anchor peer update for SG  MSP   ##########"
  echo "#################################################################"
  set -x
  docker run -it --rm -v ${PWD}:/workdir --workdir /workdir hyperledger/fabric-tools:$IMAGETAG configtxgen -configPath /workdir -profile EnergyChannel -outputAnchorPeersUpdate $CHANNEL_ARTIFACTS_DIR/SGMSPanchors.tx -channelID $CHANNEL_NAME -asOrg SGMSP
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate anchor peer update for SG MSP..."
    exit 1
  fi
  echo  

  echo
  echo "#################################################################"
  echo "#######    Generating anchor peer update for SW  MSP   ##########"
  echo "#################################################################"
  set -x
  docker run -it --rm -v ${PWD}:/workdir --workdir /workdir hyperledger/fabric-tools:$IMAGETAG configtxgen -configPath /workdir -profile EnergyChannel -outputAnchorPeersUpdate $CHANNEL_ARTIFACTS_DIR/SWMSPanchors.tx -channelID $CHANNEL_NAME -asOrg SWMSP
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate anchor peer update for SW MSP..."
    exit 1
  fi
  echo
}

# Versions of fabric known not to work with this release of first-network
BLACKLISTED_VERSIONS="^1\.0\. ^1\.1\.0-preview ^1\.1\.0-alpha"

# Do some basic sanity checking to make sure that the appropriate versions of fabric
# binaries/images are available.  In the future, additional checking for the presence
# of go or other items could be added.
function checkPrereqs() {
  # Note, we check configtxlator externally because it does not require a config file, and peer in the
  # docker image because of FAB-8551 that makes configtxlator return 'development version' in docker
  #LOCAL_VERSION=$(configtxlator version | sed -ne 's/ Version: //p')
  DOCKER_IMAGE_VERSION=$(docker run --rm hyperledger/fabric-tools:$IMAGETAG peer version | sed -ne 's/ Version: //p' | head -1)

  echo "LOCAL_VERSION=$LOCAL_VERSION"
  echo "DOCKER_IMAGE_VERSION=$DOCKER_IMAGE_VERSION"

  if [ "$LOCAL_VERSION" != "$DOCKER_IMAGE_VERSION" ]; then
    echo "=================== WARNING ==================="
    echo "  Local fabric binaries and docker images are  "
    echo "  out of  sync. This may cause problems.       "
    echo "==============================================="
  fi

  for UNSUPPORTED_VERSION in $BLACKLISTED_VERSIONS; do
    echo "$LOCAL_VERSION" | grep -q $UNSUPPORTED_VERSION
    if [ $? -eq 0 ]; then
      echo "ERROR! Local Fabric binary version of $LOCAL_VERSION does not match"
      exit 1
    fi

    echo "$DOCKER_IMAGE_VERSION" | grep -q $UNSUPPORTED_VERSION
    if [ $? -eq 0 ]; then
      echo "ERROR! Fabric Docker image version of $DOCKER_IMAGE_VERSION does not match"
      exit 1
    fi
  done
}

localNetworkUp() {
  checkPrereqs
  
  #cd ./chaincode/token && go mod vendor
  cd ./chaincode/token
  cd ../../

  COMPOSE_FILES="-f ${COMPOSE_CLI_FILE} -f ${COMPOSE_LOCAL_FILE} -f ${COMPOSE_COUCH_FILE} -f ${COMPOSE_CA_FILE}"

  export ENITT_CA_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/enitt.energy.org/ca && ls *_sk)
  export SC_CA_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/sc.energy.org/ca && ls *_sk)
  export SG_CA_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/sg.energy.org/ca && ls *_sk)
  export SW_CA_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/sw.energy.org/ca && ls *_sk)

  SYS_CHANNEL=$SYS_CHANNEL IMAGE_TAG=$IMAGETAG docker-compose $COMPOSE_FILES up -d 2>&1

  sleep 1
  echo "Sleeping 15s to allow raft cluster to complete booting"
  sleep 14

}

createAndJoinChannel() {
  # now run the end to end script
  docker exec cli scripts/script.sh $CHANNEL_NAME $CLI_DELAY
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Create and join channel failed"
    exit 1
  fi
}

# buildChaincode() {

# }

# Obtain CONTAINER_IDS and remove them
# TODO Might want to make this optional - could clear other containers
function clearContainers() {
  CONTAINER_IDS=$(docker ps -a | awk '($2 ~ /dev-peer.*.*.*/) {print $1}')
  if [ -z "$CONTAINER_IDS" -o "$CONTAINER_IDS" == " " ]; then
    echo "---- No containers available for deletion ----"
  else
    docker rm -f $CONTAINER_IDS
  fi
}

# Delete any images that were generated as a part of this setup
# specifically the following images are often left behind:
# TODO list generated image naming patterns
function removeUnwantedImages() {
  DOCKER_IMAGE_IDS=$(docker images | awk '($1 ~ /dev-peer.*.*.*/) {print $3}')
  if [ -z "$DOCKER_IMAGE_IDS" -o "$DOCKER_IMAGE_IDS" == " " ]; then
    echo "---- No images available for deletion ----"
  else
    docker rmi -f $DOCKER_IMAGE_IDS
  fi
}

function networkDown() {

  docker-compose -f $COMPOSE_CLI_FILE -f $COMPOSE_LOCAL_FILE -f $COMPOSE_COUCH_FILE -f $COMPOSE_CA_FILE down --volumes --remove-orphans

  # Don't remove the generated artifacts -- note, the ledgers are always removed
  if [ "$MODE" != "restart" ]; then
    # Bring down the network, deleting the volumes
    #Delete any ledger backups
    docker run -v $PWD:/tmp/first-network --rm hyperledger/fabric-tools:$IMAGETAG rm -Rf /tmp/first-network/ledgers-backup
    #Cleanup the chaincode containers
    clearContainers
    #Cleanup images
    removeUnwantedImages
    # remove orderer block and other channel configuration transactions and certs
    # rm -rf channel-artifacts/*.block channel-artifacts/*.tx crypto-config ./org3-artifacts/crypto-config/ channel-artifacts/org3.json
    # remove the docker-compose yaml file that was customized to the example
    # rm -f docker-compose-e2e.yaml
  fi
}

clean() {
  echo "Clean Docker"
  docker kill $(docker ps -aq)
  docker rm $(docker ps -aq)
  rm -rf /var/hyperledger/*
  #   docker rmi $(docker images -aq) -f
}

# then parse opts
while getopts "h?cjtld" opt; do
  case "$opt" in
  h | \?)
    printHelp
    exit 0
    ;;
  c)
    generateCryptoCredentials
    generateChannelArtifacts
    ;;
  j)
    createAndJoinChannel
    ;;
  t)
    # createAndJoinChannel
    ;;
  l)
    localNetworkUp
    createAndJoinChannel
    ;;

  d)
    networkDown
    ;;
  esac
done
