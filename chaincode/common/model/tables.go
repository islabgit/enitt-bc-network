package model

import (
	"math/big"
)

// Private User Info
type User struct {
	KeyType       KeyType  `json:"key_type"`
	UID           string   `json:"uid"`
	Password      []byte   `json:"pw"`
	Certification []byte   `json:"certification"`
	CACertification []byte   `json:"cacertification"`
	IoTPlatforms  []string `json:"iot_platforms"`
	CreatedAt     int64    `json:"created_at"`
}

type IoTPlatform struct {
	KeyType         KeyType  `json:"key_type"`
	UID             string   `json:"uid"`
	Certification   []byte   `json:"certification"`
	Parent          string   `json:"parent"`
	IoTDevices      []string `json:"iot_devices"`
	CreatedAt       int64    `json:"created_at"`
}

//TODO : need to inheritance structure for resource-devicetype(camera ...)
type IoTDevice struct {
	KeyType         KeyType `json:"key_type"`
	UID             string  `json:"uid"`
	Nickname        string  `json:"nickname"`
	Certification   []byte  `json:"certification"`
	Parent          string  `json:"parent"`
	Location        string  `json:"location"`
	Description     string  `json:"description"`
	ResourceAddress string  `json:"resource_address"`
	CreatedAt       int64   `json:"created_at"`
}

type ECDSASignature struct {
	R, S *big.Int
}

type Signature interface {
	Verify() bool
}
