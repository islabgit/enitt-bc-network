package model

type KeyType string

var (
	KeyTypeMap = map[string]KeyType{
		"userType":             UserType,
		"iotDeviceType":        IoTDeviceType,
		"iotPlatformType":      IoTPlatformType,
		"tokenType":            TokenType,
		"verificationCodeType": VerificationCodeType,
		"temporalType":         TemporalType,
	}
)

const (
	UserType             KeyType = "user_"
	IoTDeviceType        KeyType = "iotd_"
	IoTPlatformType      KeyType = "iotp_"
	TokenType            KeyType = "token_"
	VerificationCodeType KeyType = "vcode_"
	TemporalType         KeyType = "tmp_"
)
