package model

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
)

func VerifySignature(certification []byte, signature string, data []byte) bool {
	// func (s ECDSASignature) Verify(certification []byte, data []byte) bool {
	cert, _ := x509.ParseCertificate(certification)
	publicKey := cert.PublicKey.(*ecdsa.PublicKey)

	der, err := base64.StdEncoding.DecodeString(signature)
	if err != nil {
		return false
	}

	sig := &ECDSASignature{}
	_, err = asn1.Unmarshal(der, sig)
	if err != nil {
		return false
	}

	if ecdsa.Verify(publicKey, data, sig.R, sig.S) {
		return true
	}
	return false
}

func RemoveItemByIndex(arr []string, index int) []string {
	return append(arr[:index], arr[index+1:]...)
}
