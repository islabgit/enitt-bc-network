package common

import (
	"fmt"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"log"
	"math/big"
	"strings"
	"time"
	//"net"
)

func MakeUID(id []string) string {
	return strings.Join(id, "_")
}

func Encode(privateKey *ecdsa.PrivateKey, publicKey *ecdsa.PublicKey) ([]byte, []byte) {
	x509Encoded, _ := x509.MarshalECPrivateKey(privateKey)
	pemEncoded := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded})

	x509EncodedPub, _ := x509.MarshalPKIXPublicKey(publicKey)
	pemEncodedPub := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})

	return pemEncoded, pemEncodedPub
}

func Decode(pemEncoded string, pemEncodedPub string) (*ecdsa.PrivateKey, *ecdsa.PublicKey) {
	block, _ := pem.Decode([]byte(pemEncoded))
	x509Encoded := block.Bytes
	privateKey, _ := x509.ParseECPrivateKey(x509Encoded)

	blockPub, _ := pem.Decode([]byte(pemEncodedPub))
	x509EncodedPub := blockPub.Bytes
	genericPublicKey, _ := x509.ParsePKIXPublicKey(x509EncodedPub)
	publicKey := genericPublicKey.(*ecdsa.PublicKey)

	return privateKey, publicKey
}

func SHA256Hash(str string) []byte {
	res := sha256.New()
	res.Write([]byte(str))
	return res.Sum(nil)
}

func ExtractPrivateKey(param []byte) []byte {
	block, _ := pem.Decode(param)
	x509Encoded := block.Bytes
	privateKey, _ := x509.ParseECPrivateKey(x509Encoded)

	fmt.Println(privateKey)

	x509Encoded2, _ := x509.MarshalECPrivateKey(privateKey)
	pemEncoded := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded2})

	return pemEncoded
}

func CreateCertification(privateKey []byte, publicKey string, serialNumberBytes []byte, caCRT []byte) []byte {

	block, _ := pem.Decode(privateKey)
	x509Encoded := block.Bytes
	priv, _ := x509.ParseECPrivateKey(x509Encoded)
	pcrt, _ := x509.ParseCertificate(caCRT)
	blockPub, _ := pem.Decode([]byte(publicKey))
	x509EncodedPub := blockPub.Bytes
	genericPublicKey, _ := x509.ParsePKIXPublicKey(x509EncodedPub)
	pub := genericPublicKey.(*ecdsa.PublicKey)

	serialNumber := new(big.Int)

	template := x509.Certificate{
		SerialNumber: serialNumber.SetBytes(serialNumberBytes),		
		Subject: pkix.Name{			
			Organization: []string{"ENITT"},
			CommonName: "localhost",
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().AddDate(10, 0, 0),
		IsCA: false,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		//KeyUsage:              x509.KeyUsageCRLSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		//BasicConstraintsValid: true,
		//IPAddresses:           []net.IP{net.ParseIP("127.0.0.1")},
		MaxPathLenZero: true,
	}
	
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, pcrt, pub, priv)
	//derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pub, priv)

	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	return derBytes
}

//self-sign CA
func CreateCertification2(privateKey []byte, publicKey []byte, serialNumberBytes []byte) []byte {

	block, _ := pem.Decode(privateKey)
	x509Encoded := block.Bytes
	priv, _ := x509.ParseECPrivateKey(x509Encoded)
	blockPub, _ := pem.Decode(publicKey)
	x509EncodedPub := blockPub.Bytes
	genericPublicKey, _ := x509.ParsePKIXPublicKey(x509EncodedPub)
	pub := genericPublicKey.(*ecdsa.PublicKey)

	serialNumber := new(big.Int)

	template := x509.Certificate{
		SerialNumber: serialNumber.SetBytes(serialNumberBytes),
		Subject: pkix.Name{
			Organization: []string{"ENITT"},
			CommonName: "localhost",
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().AddDate(10, 0, 0),
		IsCA: true,
		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		//KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		//IPAddresses:           []net.IP{net.ParseIP("127.0.0.1")},
		MaxPathLen: 1,
		MaxPathLenZero:        false,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pub, priv)	

	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	return derBytes
}

func ExtractPublicKeyFromCert(certPEM []byte) *ecdsa.PublicKey {
	block, _ := pem.Decode(certPEM)
	cert, _ := x509.ParseCertificate(block.Bytes)

	publicKey := cert.PublicKey.(*ecdsa.PublicKey)
	// x509EncodedPub, _ := x509.MarshalPKIXPublicKey(publicKey)
	// pemEncodedPub := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})

	return publicKey
}
