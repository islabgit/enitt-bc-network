package erc20

import (
	"fmt"
	
//	"encoding/json"
//	"github.com/hyperledger/fabric/common/metrics/disabled"
//	"github.com/hyperledger/fabric/core/ledger/util/couchdb"
	"crypto/aes"
	//"strconv"
	"github.com/pkg/errors"
	"github.com/s7techlab/cckit/extensions/owner"
	"github.com/s7techlab/cckit/router"
	p "github.com/s7techlab/cckit/router/param"
)

const SymbolKey = `symbol`
const NameKey = `name`
const TotalSupplyKey = `totalSupply`
const OwnerMSP = `ENITTMSP`
const OwnerID = `admin`

/*
type (
	ddoc struct {
		Index 	field `json:"index"` 
		DDoc    string `json:"buyerId"`
		Name    string `json:"sellerId"`
		Type    string `json:"amount"`
	}
	field struct {
		Fields []string `json:"fields"`
	}	
)

func newDDoc(name string, fields ...string) *ddoc {
	return &ddoc{
		Index: field{
			Fields: append([]string{}, fields...),
		},
		DDoc: name + "DDoc",
		Name: name,
		Type: "json",
	}
}

func indexies() []*ddoc {
	return []*ddoc{
		newDDoc("indexIncentiveDoc", "docType", "time"),
	}
}

func indexing() {
	couchDBDef := couchdb.GetCouchDBDefinition()
	couchInstance, err := couchdb.CreateCouchInstance(couchDBDef.URL, couchDBDef.Username, couchDBDef.Password, couchDBDef.MaxRetries, couchDBDef.MaxRetriesOnStartup, couchDBDef.RequestTimeout, couchDBDef.CreateGlobalChangesDB, &disabled.Provider{})
	
	if err != nil {
		fmt.Printf("[indexing] first step err: %s", err)
		fmt.Printf("[indexing] couchDBDef info: %s + %s + %s + %s + %s + %s + %s", couchDBDef.URL, couchDBDef.Username, couchDBDef.Password, couchDBDef.MaxRetries, couchDBDef.MaxRetriesOnStartup, couchDBDef.RequestTimeout, couchDBDef.CreateGlobalChangesDB)
		panic(err)
	}

	chainName := "marketplace-channel"
	namespace := "indexcc"
	namespaceDBName := couchdb.ConstructNamespaceDBName(chainName, namespace)
	db, err := couchdb.CreateCouchDatabase(couchInstance, namespaceDBName)
	
	if err != nil {
		fmt.Printf("[indexing] second step err: %s", err)
		panic(err)
	}


	for _, v := range indexies() {
		b, err := json.Marshal(v)
		if err != nil {
			panic(err)
		}

		_, err = db.CreateIndex(string(b))
		if err != nil {
			panic(err)
		}
	}
}
*/

func NewErc20FixedSupply() *router.Chaincode {
	fmt.Printf("Init Power Related Chaincode \n")
	r := router.New(`erc20fixedSupply`).Use(p.StrictKnown).		

		// Chaincode init function, initiates token smart contract with token symbol, name and totalSupply
		Init(invokeInitFixedSupply, p.String(`symbol`), p.String(`name`), p.Int(`totalSupply`)).

		// Generate value amount of tokens
		Invoke(`generate`, invokeGenerate, p.Int(`ammount`)).

		// Get token symbol
		Query(`symbol`, querySymbol).

		// Get token name
		Query(`name`, queryName).

		// Get the total token supply
		Query(`totalSupply`, queryTotalSupply).

		//  get account balance
		Query(`balanceOf`, queryBalanceOf, p.String(`mspId`), p.String(`id`)).

		//  get account available Balance
		Query(`availableBalanceOf`, queryAvailableBalanceOf, p.String(`mspId`), p.String(`id`)).

		//Send value amount of tokens
		Invoke(`transfer`, invokeTransfer, p.String(`fromMspId`), p.String(`fromId`), p.String(`toMspId`), p.String(`toId`), p.Int(`amount`)).

		// Allow spender to withdraw from your account, multiple times, up to the _value amount.
		// If this function is called again it overwrites the current allowance with _valu
		Invoke(`approve`, invokeApprove, p.String(`ownerMspId`), p.String(`ownerId`), p.String(`spenderMspId`), p.String(`spenderId`), p.Int(`amount`)).

		//    Returns the amount which _spender is still allowed to withdraw from _owner]
		Query(`allowance`, queryAllowance, p.String(`ownerMspId`), p.String(`ownerId`),
			p.String(`spenderMspId`), p.String(`spenderId`)).

		// Send amount of tokens from owner account to another
		Invoke(`transferFrom`, invokeTransferFrom, p.String(`invokerMspId`), p.String(`invokerId`), p.String(`fromMspId`), p.String(`fromId`),
			p.String(`toMspId`), p.String(`toId`), p.Int(`amount`)).


		// TODO: Refactoring: Separate erc20 / power interface
		Invoke(`booking`, invokeBooking, p.String(`tradeId`), p.String(`sellerId`), p.String(`buyerId`), p.String(`power`), p.String(`price`), p.String(`time`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Invoke(`trade`, invokeTrade, p.String(`tradeId`), p.String(`sellerId`), p.String(`buyerId`), p.String(`power`), p.String(`price`), p.String(`state`), p.String(`time`)).


		// TODO: Refactoring: Separate erc20 / power interface
		Query(`bookingHistory`, queryBookingHistory, p.String(`userId`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`tradeHistory`, queryTradHistory, p.String(`userId`), p.String(`state`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`tradeByUser`, queryTradeByUser, p.String(`userId`)).		

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`tradeByTime`, queryTradeByTime, p.String(`termStart`), p.String(`termEnd`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Invoke(`transmissionPower`, invokeTransmissionPowerHistory, p.String(`transmissionId`), p.String(`femsId`), p.String(`termStart`), p.String(`termEnd`), p.String(`power`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`transmissionPowerByFEMS`, queryTransmissionPowerHistory, p.String(`femsId`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Invoke(`receivePower`, invokeReceivePowerHistory, p.String(`receiveId`), p.String(`femsId`), p.String(`termStart`), p.String(`termEnd`), p.String(`power`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`receivePowerByFEMS`, queryReceivePowerHistory, p.String(`femsId`)).

		Invoke(`initUser`, invokeInitUser, p.String(`userMspId`), p.String(`userId`))

		// TODO: Refactoring: Separate erc20 / device interface
		//Invoke(`deviceRegister`, invokeDeviceRegister, p.String(`deviceId`), p.String(`deviceName`), p.String(`publicKey`)).

		// TODO: Refactoring: Separate erc20 / device interface
		//Query(`deviceById`, queryDeviceById, p.String(`deviceId`))


	return router.NewChaincode(r)
}

func invokeInitFixedSupply(c router.Context) (interface{}, error) {	

	key := "@NcRfUjXn2r5u8x/A?D*G-KaPdSgVkYp"

	ownerIdentity, err := owner.SetFromCreator(c)
	if err != nil {
		return nil, errors.Wrap(err, `set chaincode owner`)
	}

	// save token configuration in state
	if err := c.State().Insert(SymbolKey, c.ParamString(`symbol`)); err != nil {
		fmt.Printf("token symbol already exist")
		// return nil, err
	}

	block, err := aes.NewCipher([]byte(key)) // AES 대칭키 암호화 블록 생성
	if err != nil {
		fmt.Println(err)		
	}

	ciphertext := encrypt(block, []byte(c.ParamString(`name`)))
	

	if err := c.State().Insert(NameKey, ciphertext); err != nil {
		fmt.Printf("token name already exist")
		// return nil, err
	}

	if err := c.State().Insert(TotalSupplyKey, c.ParamInt(`totalSupply`)); err != nil {
		fmt.Printf("token totalSupply already exist")
		// return nil, err
	}

	// fmt.Printf("Owner MSP : %s, Owner ID : %s \n", ownerIdentity.GetMSPID(), ownerIdentity.GetID())

	// set token owner initial balance
	fmt.Printf("\n================= Init Token System =================\n")
	fmt.Printf("Token symbol : %s \n", c.ParamString(`symbol`))
	//fmt.Printf("Token name : %s \n", c.ParamString(`name`))
	fmt.Printf("Token name : %x \n", ciphertext)
	fmt.Printf("Token total supply : %d \n", c.ParamInt(`totalSupply`))
	fmt.Printf("Owner MSPID : %s \n", OwnerMSP)
	fmt.Printf("Owner ID : %s \n", OwnerID)
	fmt.Printf("Owner Subject : %s \nOwner PEM : \n%s", ownerIdentity.GetSubject(), ownerIdentity.GetPEM())
	fmt.Printf("=====================================================\n")
	
	if err := setBalance(c, OwnerMSP, OwnerID, c.ParamInt(`totalSupply`)); err != nil {
		return nil, errors.Wrap(err, `set owner initial balance`)
	}
	
	/*
	for i := 0; i < 100; i++ {
		if err := setBalance(c, OwnerMSP, OwnerID + strconv.Itoa(i), c.ParamInt(`totalSupply`)/100); err != nil {
			return nil, errors.Wrap(err, `set owner initial balance`)
		}	
	}*/

	//this part for Test Scenario Section, should be delete

	return ownerIdentity, nil
}
