package erc20

import (
	"bytes"
	"fmt"
	"strconv"
	"encoding/json"
	"github.com/s7techlab/cckit/identity"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	r "github.com/s7techlab/cckit/router"
)

type (
	Trade struct {
		DocType 	string `json:"docType"` //docType is used to distinguish the various types of objects in state database
		BuyerId     string `json:"buyerId"`
		SellerId    string `json:"sellerId"`
		Power       string `json:"power"`
		Price		string `json:"price"`
		State       string `json:"state"` // 0: booking 1: done
		Time        string `json:"time"`
	}
	PowerHistory struct {
		DocType		string `json:"docType"` // Transmission or Receive Power 
		FemsId		string `json:"femsId"`
		TermStart   string `json:"termStart"`
		TermEnd     string `json:"termEnd"`
		Power       string `json:"power"`
	}
)

func tradeKey(num string) []string {
	return []string{num}
}
func invokeBooking(c r.Context) (interface{}, error) {
	docType := "trade"
	tradeId := c.ParamString(`tradeId`)	
	sellerId := c.ParamString(`sellerId`)
	buyerId := c.ParamString(`buyerId`)
	power := c.ParamString(`power`)
	price := c.ParamString(`price`)
	state := "0" // 0: booking 1: done
	time := c.ParamString(`time`)

	// ==== Check if booking data already exists ====
	tradeAsBytes, err := c.Stub().GetState(tradeId)
	if err != nil {
		return nil, err
	} else if tradeAsBytes != nil {
		fmt.Println("This booking data already exists: " + tradeId)
		
		return nil, err
	}

	// ==== Create trade object and marshal to JSON ====
	
	trade := &Trade{docType, buyerId, sellerId, power, price, state, time}	
	tradeJSONasBytes, err := json.Marshal(trade)
	if err != nil {
		return nil, err
	}
	
	// === Save trade history to state ===
	err = c.Stub().PutState(tradeId, tradeJSONasBytes)
	if err != nil {
		return nil, err
	}

	return true, nil

}

func invokeTrade(c r.Context) (interface{}, error) {
	msp := "ENITTMSP"

	docType := "trade"
	tradeId := c.ParamString(`tradeId`)	
	sellerId := c.ParamString(`sellerId`)
	buyerId := c.ParamString(`buyerId`)
	power := c.ParamString(`power`)
	price := c.ParamString(`price`)
	state := c.ParamString(`state`) // 0: booking, 1: process, 2: finish
	time := c.ParamString(`time`)

	// ==== Check if tade history already exists ====
	tradeAsBytes, err := c.Stub().GetState(tradeId)

	if err != nil {
		return nil, err
	} else if tradeAsBytes != nil {
		trade := &Trade{}
		json.Unmarshal(tradeAsBytes, &trade)
		trade.Power = power
		trade.Price = price
		trade.State = state
		trade.Time = time

		if state == "2" {
			amount,_ := strconv.Atoi(price)
			val,_ := strconv.Atoi(power)

			// Disallow to transfer token to same account
			if sellerId == buyerId {
				return nil, ErrForbiddenToTransferToSameAccount
			}

			// get information about invoker balance from state
			invokerBalance, err := getBalance(c, msp, buyerId)
			if err != nil {
				return nil, err
			}

			// fmt.Printf("\n ================= Invoke Transfer =================\n")
			// fmt.Printf("To MSPID : %s \n", toMspId)
			// fmt.Printf("To ID : %s \n", toId)
			// fmt.Printf("Invoker MSPID : %s \n", fromMspId)
			// fmt.Printf("Invoker ID : %s \n", fromId)
			// fmt.Printf("Invoker Subject : %s \nInvoker PEM : %s \n", invoker.GetSubject(), invoker.GetPEM())
			// fmt.Printf("Invoker Balance : %d \n", invokerBalance)
			// fmt.Printf("invokerBalance-amount : %d \n", invokerBalance-amount)

			// fmt.Printf("\n =====================================================\n")

			// Check the funds sufficiency
			if invokerBalance-(amount*val) < 0 {
				return nil, ErrNotEnoughFunds
			}

			// Get information about recipient balance from state
			recipientBalance, err := getBalance(c, msp, sellerId)
			if err != nil {
				return nil, err
			}

			// Update payer and recipient balance
			if err = setBalance(c, msp, buyerId, invokerBalance-(amount*val)); err != nil {
				return nil, err
			}

			if err = setBalance(c, msp, sellerId, recipientBalance+(amount*val)); err != nil {
				return nil, err
			}

			// Trigger event with name "trade" and payload - serialized to json Transfer structure
			if err = c.SetEvent(`transfer`, &Transfer{
				From: identity.Id{
					MSP:  msp,
					Cert: sellerId,
				},
				To: identity.Id{
					MSP:  msp,
					Cert: buyerId,
				},
				Amount: amount,
			}); err != nil {
				return nil, err
			}
		}

		tradeAsBytes, _ = json.Marshal(trade)
		s_err := c.Stub().PutState(tradeId, tradeAsBytes)

		if s_err != nil {
			return nil, s_err
		}

		fmt.Println("Booking data updated: " + tradeId)
		fmt.Println("updated data: %s", trade)
		
		return true, err
	}

	// ==== Create trade object and marshal to JSON ====
	
	trade := &Trade{docType, buyerId, sellerId, power, price, state, time}
	tradeJSONasBytes, err := json.Marshal(trade)
	if err != nil {
		return nil, err
	}
	
	// === Save trade history to state ===
	err = c.Stub().PutState(tradeId, tradeJSONasBytes)
	if err != nil {
		return nil, err
	}

	return true, nil
}

func queryBookingHistory(c r.Context) (interface{}, error) {
	
	docType := "trade"
	userId := c.ParamString(`userId`)

	queryString := ""

	if userId == "*" {
		queryString = fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"$or\":[{\"state\":\"%s\"}]}}", docType, "0")
	} else {
		queryString = fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"$and\":[{\"$or\":[{\"buyerId\":\"%s\"},{\"sellerId\":\"%s\"}]}, {\"state\":\"%s\"}]}}", docType, userId, userId, "0")
	}

	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)

	if err != nil {
		return nil, err
	}

	return queryResults, nil
}

func queryTradHistory(c r.Context) (interface{}, error) {
	
	docType := "trade"
	userId  := c.ParamString(`userId`)	
	state   := c.ParamString(`state`)	
	queryString := ""

	
	if userId == "*" {
		queryString = fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"$or\":[{\"state\":\"%s\"}]}}", docType, state)
	} else {
		queryString = fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"$and\":[{\"$or\":[{\"buyerId\":\"%s\"},{\"sellerId\":\"%s\"}]}, {\"state\":\"%s\"}]}}", docType, userId, userId, state)
	}	
	
	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	return queryResults, nil
}


func queryTradeByUser(c r.Context) (interface{}, error) {
	
	docType := "trade"
	userId := c.ParamString(`userId`)

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"$or\":[{\"buyerId\":\"%s\"},{\"sellerId\":\"%s\"}]}}", docType, userId, userId)

	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	return queryResults, nil
}

func queryTradeByTime(c r.Context) (interface{}, error) {
	docType := "trade"
	termStart := c.ParamString(`termStart`)
	termEnd := c.ParamString(`termEnd`)

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"$and\":[{\"time\":{\"$gte\":\"%s\"}},{\"time\":{\"$lte\":\"%s\"}}]}}", docType, termStart, termEnd)

	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	return queryResults, nil
}

func invokeTransmissionPowerHistory(c r.Context) (interface{}, error) {
	docType := "transmission"
	transmissionId := c.ParamString(`transmissionId`)
	femsId := c.ParamString(`femsId`)
	termStart := c.ParamString(`termStart`)
	termEnd := c.ParamString(`termEnd`)
	power := c.ParamString(`power`)

	// ==== Check if transmission history already exists ====
	transmissionAsBytes, err := c.Stub().GetState(transmissionId)
	if err != nil {
		return nil, err
	} else if transmissionAsBytes != nil {
		fmt.Println("This transmission history already exists: " + transmissionId)
		
		return nil, err
	}

	// ==== Create transmission object and marshal to JSON ====
	
	transmission := &PowerHistory{docType, femsId, termStart, termEnd, power}
	fmt.Println("transmission power:" + power)
	
	transmissionJSONasBytes, err := json.Marshal(transmission)
	if err != nil {
		return nil, err
	}
	
	// === Save trade history to state ===
	err = c.Stub().PutState(transmissionId, transmissionJSONasBytes)
	if err != nil {
		return nil, err
	}

	return true, nil

}

func queryTransmissionPowerHistory(c r.Context) (interface{}, error) {
	docType := "transmission"
	femsId := c.ParamString(`femsId`)

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"femsId\":\"%s\"}}", docType, femsId)

	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	return queryResults, nil
}

func invokeReceivePowerHistory(c r.Context) (interface{}, error) {
	docType := "receive"
	receiveId := c.ParamString(`receiveId`)
	femsId := c.ParamString(`femsId`)
	termStart := c.ParamString(`termStart`)
	termEnd := c.ParamString(`termEnd`)
	power := c.ParamString(`power`)

	// ==== Check if receive history already exists ====
	receiveAsBytes, err := c.Stub().GetState(receiveId)
	if err != nil {
		return nil, err
	} else if receiveAsBytes != nil {
		fmt.Println("This transmission history already exists: " + receiveId)
		
		return nil, err
	}

	// ==== Create receive object and marshal to JSON ====
	
	receive := &PowerHistory{docType, femsId, termStart, termEnd, power}
	receiveJSONasBytes, err := json.Marshal(receive)
	if err != nil {
		return nil, err
	}
	
	// === Save trade history to state ===
	err = c.Stub().PutState(receiveId, receiveJSONasBytes)
	if err != nil {
		return nil, err
	}

	return true, nil
}

func queryReceivePowerHistory(c r.Context) (interface{}, error) {
	docType := "receive"
	femsId := c.ParamString(`femsId`)

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"femsId\":\"%s\"}}", docType, femsId)

	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	return queryResults, nil
}



func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) (interface{}, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return nil, err
	}

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.String(), nil
}

// ===========================================================================================
// constructQueryResponseFromIterator constructs a JSON array containing query results from
// a given result iterator
// ===========================================================================================
func constructQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) {
	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"id\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	return &buffer, nil
}
