package erc20

import (
	"fmt"
	"encoding/json"
	"bytes"
	"strconv"
	"io"
	"crypto/cipher"
	"crypto/rand"	
	"crypto/aes"
	
	"github.com/pkg/errors"
	"github.com/s7techlab/cckit/convert"
	"github.com/s7techlab/cckit/identity"
	r "github.com/s7techlab/cckit/router"	
)

const (
	BalancePrefix   = `BALANCE`
	AllowancePrefix = `APPROVE`
)

var (
	ErrNotEnoughFunds                   = errors.New(`not enough funds`)
	ErrForbiddenToTransferToSameAccount = errors.New(`forbidden to transfer to same account`)
	ErrSpenderNotHaveAllowance          = errors.New(`spender not have allowance for amount`)
)

type (
	Data struct {		
		ID      string `json:"Key"`
		Trade 	Trade  `json:"Record"` // query with trade data
	}

	Transfer struct {
		From   identity.Id
		To     identity.Id
		Amount int
	}

	Approve struct {
		From    identity.Id
		Spender identity.Id
		Amount  int
	}

)

func invokeGenerate(c r.Context) (interface{}, error) {	
	totalVal, _ := c.State().GetInt(TotalSupplyKey, 0)
	totalVal += c.ParamInt(`ammount`)


	if err := setBalance(c, OwnerMSP, OwnerID, totalVal); err != nil {
		return nil, errors.Wrap(err, `set owner initial balance`)
	}

	return totalVal, nil
}

func invokeInitUser(c r.Context) (interface{}, error) {	
		// transfer target
		userMspId := c.ParamString(`userMspId`)
		userId := c.ParamString(`userId`)


	if err := setBalance(c, userMspId, userId, 0); err != nil {
		return nil, errors.Wrap(err, `init user`)
	}

	return true, nil
}

func querySymbol(c r.Context) (interface{}, error) {
	return c.State().Get(SymbolKey)
}

func queryName(c r.Context) (interface{}, error) {
	key := "@NcRfUjXn2r5u8x/A?D*G-KaPdSgVkYp"
	block, err := aes.NewCipher([]byte(key)) // AES 대칭키 암호화 블록 생성
	if err != nil {
		fmt.Println(err)		
	}
	s, _ := c.State().Get(NameKey, convert.TypeString)
	//s := fmt.Sprint(tmp)
	
	plaintext := decrypt(block, []byte(s.(string)))

	return string(plaintext), err
}

func queryTotalSupply(c r.Context) (interface{}, error) {
	return c.State().Get(TotalSupplyKey)
}

func queryBalanceOf(c r.Context) (interface{}, error) {	
	return getBalance(c, c.ArgString(`mspId`), c.ArgString(`id`))
}

func queryAvailableBalanceOf(c r.Context) (interface{}, error) {
	totalBalance, err := getBalance(c, c.ArgString(`mspId`), c.ArgString(`id`))

	if totalBalance > 0 {
		data := make([]Data,0)
		docType := "trade"
		queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"$and\":[{\"$or\":[{\"buyerId\":\"%s\"}]}, {\"state\":\"%s\"}]}}", docType, c.ArgString(`id`), "0")

		resultsIterator, err := c.Stub().GetQueryResult(queryString)
		defer resultsIterator.Close()
		if err != nil {
			return nil, err
		}
		// buffer is a JSON array containing QueryRecords
		var buffer bytes.Buffer
		buffer.WriteString("[")
		bArrayMemberAlreadyWritten := false
		for resultsIterator.HasNext() {
			queryResponse,
			err := resultsIterator.Next()
			if err != nil {
				return nil, err
			}
			// Add a comma before array members, suppress it for the first array member
			if bArrayMemberAlreadyWritten == true {
				buffer.WriteString(",")
			}
			buffer.WriteString("{\"Key\":")
			buffer.WriteString("\"")
			buffer.WriteString(queryResponse.Key)
			buffer.WriteString("\"")
			buffer.WriteString(", \"Record\":")
			// Record is a JSON object, so we write as-is
			buffer.WriteString(string(queryResponse.Value))
			buffer.WriteString("}")
			bArrayMemberAlreadyWritten = true
		}
		buffer.WriteString("]")
		fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())


		//queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
		//tradeJSONasBytes, err := json.Marshal(queryResults)		
		json.Unmarshal([]byte(buffer.String()), &data)

		//fmt.Println(queryResults)

		if err != nil {			
			fmt.Println("Error: ", err)
		}

		for _, v := range data {			
			//totalBalance += 1000000
			tmp, _ := strconv.Atoi(v.Trade.Price)
			tmp2, _ := strconv.Atoi(v.Trade.Power)
					
			//chk += v.Trade.Price
			totalBalance -= (tmp*tmp2)
		}		
				

		return totalBalance, err


	} else {		
		return totalBalance, err
	}	
}

func invokeTransfer(c r.Context) (interface{}, error) {
	// transfer target
	fromMspId := c.ParamString(`fromMspId`)
	fromId := c.ParamString(`fromId`)
	toMspId := c.ParamString(`toMspId`)
	toId := c.ParamString(`toId`)

	//transfer amount
	amount := c.ParamInt(`amount`)

	// get information about tx creator
	// invoker, err := identity.FromStub(c.Stub())
	// if err != nil {
	// 	return nil, err
	// }

	// Disallow to transfer token to same account
	if fromMspId == toMspId && fromId == toId {
		return nil, ErrForbiddenToTransferToSameAccount
	}

	// get information about invoker balance from state
	invokerBalance, err := getBalance(c, fromMspId, fromId)
	if err != nil {
		return nil, err
	}

	// fmt.Printf("\n ================= Invoke Transfer =================\n")
	// fmt.Printf("To MSPID : %s \n", toMspId)
	// fmt.Printf("To ID : %s \n", toId)
	// fmt.Printf("Invoker MSPID : %s \n", fromMspId)
	// fmt.Printf("Invoker ID : %s \n", fromId)
	// fmt.Printf("Invoker Subject : %s \nInvoker PEM : %s \n", invoker.GetSubject(), invoker.GetPEM())
	// fmt.Printf("Invoker Balance : %d \n", invokerBalance)
	// fmt.Printf("invokerBalance-amount : %d \n", invokerBalance-amount)

	// fmt.Printf("\n =====================================================\n")

	// Check the funds sufficiency
	if invokerBalance-amount < 0 {
		return nil, ErrNotEnoughFunds
	}

	// Get information about recipient balance from state
	recipientBalance, err := getBalance(c, toMspId, toId)
	if err != nil {
		return nil, err
	}

	// Update payer and recipient balance
	if err = setBalance(c, fromMspId, fromId, invokerBalance-amount); err != nil {
		return nil, err
	}

	if err = setBalance(c, toMspId, toId, recipientBalance+amount); err != nil {
		return nil, err
	}

	// Trigger event with name "transfer" and payload - serialized to json Transfer structure
	if err = c.SetEvent(`transfer`, &Transfer{
		From: identity.Id{
			MSP:  fromMspId,
			Cert: fromId,
		},
		To: identity.Id{
			MSP:  toMspId,
			Cert: toId,
		},
		Amount: amount,
	}); err != nil {
		return nil, err
	}

	// return current invoker balance
	return invokerBalance - amount, nil
}

func queryAllowance(c r.Context) (interface{}, error) {
	return getAllowance(c, c.ParamString(`ownerMspId`), c.ParamString(`ownerId`), c.ParamString(`spenderMspId`), c.ParamString(`spenderId`))
}

func invokeApprove(c r.Context) (interface{}, error) {
	ownerMspId := c.ParamString(`ownerMspId`)
	ownerId := c.ParamString(`ownerId`)
	spenderMspId := c.ParamString(`spenderMspId`)
	spenderId := c.ParamString(`spenderId`)
	amount := c.ParamInt(`amount`)

	_, err := identity.FromStub(c.Stub())
	if err != nil {
		return nil, err
	}

	if err = setAllowance(c, ownerMspId, ownerId, spenderMspId, spenderId, amount); err != nil {
		return nil, err
	}

	if err = c.SetEvent(`approve`, &Approve{
		From: identity.Id{
			MSP:  ownerMspId,
			Cert: ownerId,
		},
		Spender: identity.Id{
			MSP:  spenderMspId,
			Cert: spenderId,
		},
		Amount: amount,
	}); err != nil {
		return nil, err
	}

	return true, nil
}

func invokeTransferFrom(c r.Context) (interface{}, error) {

	invokerMspId := c.ParamString(`invokerMspId`)
	invokerId := c.ParamString(`invokerId`)
	fromMspId := c.ParamString(`fromMspId`)
	fromId := c.ParamString(`fromId`)
	toMspId := c.ParamString(`toMspId`)
	toId := c.ParamString(`toId`)
	amount := c.ParamInt(`amount`)

	_, err := identity.FromStub(c.Stub())
	if err != nil {
		return nil, err
	}

	// check method invoker has allowances
	allowance, err := getAllowance(c, fromMspId, fromId, invokerMspId, invokerId)
	if err != nil {
		return nil, err
	}

	// transfer amount must be less or equal allowance
	if allowance < amount {
		return nil, ErrSpenderNotHaveAllowance
	}

	// current payer balance
	balance, err := getBalance(c, fromMspId, fromId)
	if err != nil {
		return nil, err
	}

	// payer balance must be greater or equal amount
	if balance-amount < 0 {
		return nil, ErrNotEnoughFunds
	}

	// current recipient balance
	recipientBalance, err := getBalance(c, toMspId, toId)
	if err != nil {
		return nil, err
	}

	// decrease payer balance
	if err = setBalance(c, fromMspId, fromId, balance-amount); err != nil {
		return nil, err
	}

	// increase recipient balance
	if err = setBalance(c, toMspId, toId, recipientBalance+amount); err != nil {
		return nil, err
	}

	// decrease invoker allowance
	if err = setAllowance(c, fromMspId, fromId, invokerMspId, invokerId, allowance-amount); err != nil {
		return nil, err
	}

	if err = c.Event().Set(`transfer`, &Transfer{
		From: identity.Id{
			MSP:  fromMspId,
			Cert: fromId,
		},
		To: identity.Id{
			MSP:  toMspId,
			Cert: toId,
		},
		Amount: amount,
	}); err != nil {
		return nil, err
	}

	// return current invoker balance
	return balance - amount, nil
}


func encrypt(b cipher.Block, plaintext []byte) []byte {
	if mod := len(plaintext) % aes.BlockSize; mod != 0 { // 블록 크기의 배수가 되어야함
		padding := make([]byte, aes.BlockSize-mod)   // 블록 크기에서 모자라는 부분을
		plaintext = append(plaintext, padding...)    // 채워줌
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext)) // 초기화 벡터 공간(aes.BlockSize)만큼 더 생성
	iv := ciphertext[:aes.BlockSize] // 부분 슬라이스로 초기화 벡터 공간을 가져옴
	if _, err := io.ReadFull(rand.Reader, iv); err != nil { // 랜덤 값을 초기화 벡터에 넣어줌
		fmt.Println(err)
		return nil
	}

	mode := cipher.NewCBCEncrypter(b, iv) // 암호화 블록과 초기화 벡터를 넣어서 암호화 블록 모드 인스턴스 생성
	mode.CryptBlocks(ciphertext[aes.BlockSize:], plaintext) // 암호화 블록 모드 인스턴스로
                                                                // 암호화

	return ciphertext
}

func decrypt(b cipher.Block, ciphertext []byte) []byte {
	if len(ciphertext)%aes.BlockSize != 0 { // 블록 크기의 배수가 아니면 리턴
		fmt.Println("암호화된 데이터의 길이는 블록 크기의 배수가 되어야합니다.")
		return nil
	}

	iv := ciphertext[:aes.BlockSize]        // 부분 슬라이스로 초기화 벡터 공간을 가져옴
	ciphertext = ciphertext[aes.BlockSize:] // 부분 슬라이스로 암호화된 데이터를 가져옴

	plaintext := make([]byte, len(ciphertext)) // 평문 데이터를 저장할 공간 생성
	mode := cipher.NewCBCDecrypter(b, iv)      // 암호화 블록과 초기화 벡터를 넣어서
                                                   // 복호화 블록 모드 인스턴스 생성
	mode.CryptBlocks(plaintext, ciphertext)    // 복호화 블록 모드 인스턴스로 복호화

	return plaintext
}

// === internal functions, not "public" chaincode functions

// setBalance puts balance value to state
func balanceKey(mspId, userId string) []string {
	// fmt.Printf("Balance key : %s \n", []string{BalancePrefix, mspId, userId})
	return []string{BalancePrefix, mspId, userId}
}

func allowanceKey(ownerMspId, ownerId, spenderMspId, spenderId string) []string {
	// fmt.Printf("Allowance key : %s \n", []string{AllowancePrefix, ownerMspId, ownerId, spenderMspId, spenderId})
	return []string{AllowancePrefix, ownerMspId, ownerId, spenderMspId, spenderId}
}

func getBalance(c r.Context, mspId, userId string) (int, error) {
	return c.State().GetInt(balanceKey(mspId, userId), 0)
}

// setBalance puts balance value to state
func setBalance(c r.Context, mspId, userId string, balance int) error {
	return c.State().Put(balanceKey(mspId, userId), balance)
}

func getAllowance(c r.Context, ownerMspId, ownerId, spenderMspId, spenderId string) (int, error) {
	return c.State().GetInt(allowanceKey(ownerMspId, ownerId, spenderMspId, spenderId), 0)
}

func setAllowance(c r.Context, ownerMspId, ownerId, spenderMspId, spenderId string, amount int) error {
	return c.State().Put(allowanceKey(ownerMspId, ownerId, spenderMspId, spenderId), amount)
}