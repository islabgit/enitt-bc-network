package main

import (
	"bytes"
	
	"crypto/ecdsa"
	"crypto/elliptic"
	
	"crypto/rand"	
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"
	"sync"
	//"io/ioutil"	

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"enitt-bc/chaincode/common"
	"enitt-bc/chaincode/common/model"			
)

var stateLock = &sync.Mutex{}

type SecurityPlaneChaincode struct {
	stub     shim.ChaincodeStubInterface
	function string
	args     []string
}

func (spc *SecurityPlaneChaincode) call() pb.Response {
	function := spc.function

	callMap := map[string]func() pb.Response{
		"enrollUser":                       spc.enrollUser,
		"enrollIoTPlatform":                spc.enrollIoTPlatform,
		"enrollIoTDevice":                  spc.enrollIoTDevice,
		"deployUserToken":                  spc.deployUserToken,
		"depolyIoTPlatformToken":           spc.depolyIoTPlatformToken,
		"depolyIoTDeviceToken":             spc.depolyIoTDeviceToken,
		"deployCodeInUser":                 spc.deployCodeInUser,
		"deployCodeInIoTPlatform":          spc.deployCodeInIoTPlatform,
		"deployCodeInIoTDevice":            spc.deployCodeInIoTDevice,
		"reEnrollUser":                     spc.reEnrollUser,
		"removeIoTPlatform":                spc.removeIoTPlatform,
		"removeIoTDevice":                  spc.removeIoTDevice,
		"removeUserToken":                  spc.removeUserToken,
		"removeIoTPlatformToken":           spc.removeIoTPlatformToken,
		"removeIoTDeviceToken":             spc.removeIoTDeviceToken,
		"removeUserCode":                   spc.removeUserCode,
		"removeIoTPlatformCode":            spc.removeIoTPlatformCode,
		"removeIoTDeviceCode":              spc.removeIoTDeviceCode,
		"isExistUser":                      spc.isExistUser,
		"isExistIoTPlatform":               spc.isExistIoTPlatform,
		"isExistIoTDevice":                 spc.isExistIoTDevice,
		"isValidUserToken":                 spc.isValidUserToken,
		"isValidIoTPlatformToken":          spc.isValidIoTPlatformToken,
		"isValidIoTDeviceToken":            spc.isValidIoTDeviceToken,
		"isValidUserCode":                  spc.isValidUserCode,
		"isValidIoTPlatformCode":           spc.isValidIoTPlatformCode,
		"isValidIoTDeviceCode":             spc.isValidIoTDeviceCode,
		"isValidIoTPlatformSignature":      spc.isValidIoTPlatformSignature,
		"isValidIoTDeviceSignature":        spc.isValidIoTDeviceSignature,
		"getUserTokenByID":                 spc.getUserTokenByID,
		"getIoTPlatformTokenWithSignature": spc.getIoTPlatformTokenWithSignature,
		"getIoTDeviceTokenWithSignature":   spc.getIoTDeviceTokenWithSignature,
		"getUserInformation":               spc.getUserInformation,
		"getIoTPlatformInformation":        spc.getIoTPlatformInformation,
		"getIoTDeviceInformation":          spc.getIoTDeviceInformation,
		"getIoTPlatformsByUserID":          spc.getIoTPlatformsByUserID,
		"getIoTDevicesByIoTPlatformID":     spc.getIoTDevicesByIoTPlatformID,
	}

	h := callMap[function]
	if h != nil {
		return callMap[function]()
	}

	res := make([]string, 0)
	for k := range callMap {
		res = append(res, `"`+k+`"`)
	}

	return shim.Error("Invalid invoke function name. Expecting " + strings.Join(res, ", "))
}

func (spc *SecurityPlaneChaincode) getState(typ model.KeyType, id string) ([]byte, error) {
	stateLock.Lock()
	res, err := spc.stub.GetState(string(typ) + id)
	stateLock.Unlock()
	return res, err
}

func (spc *SecurityPlaneChaincode) putState(typ model.KeyType, id string, data []byte) error {
	stateLock.Lock()
	err := spc.stub.PutState(string(typ)+id, data)
	stateLock.Unlock()
	return err
}

func (spc *SecurityPlaneChaincode) delState(typ model.KeyType, id string) error {
	stateLock.Lock()
	err := spc.stub.DelState(string(typ) + id)
	stateLock.Unlock()
	return err
}

func (spc *SecurityPlaneChaincode) makeTokenAndPutState(id string) ([]byte, error) {
	tokenBytes := make([]byte, 16)
	rand.Read(tokenBytes)

	token := hex.EncodeToString(tokenBytes)

	err := spc.putState(model.TokenType, id, tokenBytes)
	if err != nil {
		return nil, err
	}

	err = spc.putState(model.TemporalType, token, []byte(id))
	if err != nil {
		return nil, err
	}
	return tokenBytes, nil
}

func (spc *SecurityPlaneChaincode) makeCodeAndPutState(id string) ([]byte, error) {
	verificationCodeBytes := make([]byte, 16)
	rand.Read(verificationCodeBytes)

	err := spc.putState(model.VerificationCodeType, id, verificationCodeBytes)
	if err != nil {
		return nil, err
	}

	return verificationCodeBytes, nil
}

func (spc *SecurityPlaneChaincode) removeToken(uid string) error {

	tokenBytes, _ := spc.getState(model.TokenType, uid)
	if tokenBytes == nil {
		return nil
	}

	err := spc.delState(model.TokenType, uid)
	if err != nil {
		return err
	}

	err = spc.delState(model.TemporalType, hex.EncodeToString(tokenBytes))
	if err != nil {
		return err
	}
	return nil
}

func (spc *SecurityPlaneChaincode) isExistData(typ model.KeyType, id string) (bool, error) {

	val, err := spc.getState(typ, id)

	if err != nil {
		return false, err
	}

	if val != nil && len(val) > 0 {
		return true, nil
	}

	return false, nil
}

func (spc *SecurityPlaneChaincode) tokenValidation(id string, token string) bool {
	uid, _ := spc.getState(model.TemporalType, token)
	if id == string(uid) {
		return true
	}
	return false
}

func (spc *SecurityPlaneChaincode) codeValidation(id string, code string) bool {
	codeBytes, _ := spc.getState(model.VerificationCodeType, id)

	if code == hex.EncodeToString(codeBytes) {
		return true
	}
	return false
}

func (spc *SecurityPlaneChaincode) appendIoTPlatformIDToUser(userID string, platformID string) (*model.User, error) {

	userJSON, err := spc.getState(model.UserType, userID)
	if err != nil {
		return nil, err
	}

	user := model.User{}
	err = json.Unmarshal(userJSON, &user)
	if err != nil {
		return nil, err
	}

	i := sort.SearchStrings(user.IoTPlatforms, platformID)
	user.IoTPlatforms = append(user.IoTPlatforms[:i], append([]string{platformID}, user.IoTPlatforms[i:]...)...)

	userJSON, _ = json.Marshal(user)
	err = spc.putState(model.UserType, userID, userJSON)

	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (spc *SecurityPlaneChaincode) removeIoTPlatformIDInUser(userID string, platformID string) (*model.User, error) {

	userJSON, err := spc.getState(model.UserType, userID)
	if err != nil {
		return nil, err
	}

	user := model.User{}
	err = json.Unmarshal(userJSON, &user)
	if err != nil {
		return nil, err
	}

	i := sort.Search(len(user.IoTPlatforms), func(i int) bool { return platformID == user.IoTPlatforms[i] })
	if i >= len(user.IoTPlatforms) || user.IoTPlatforms[i] != platformID {
		return nil, errors.New("is not found : " + platformID)
	}

	user.IoTPlatforms = model.RemoveItemByIndex(user.IoTPlatforms, i)
	userJSON, _ = json.Marshal(user)
	err = spc.putState(model.UserType, userID, userJSON)

	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (spc *SecurityPlaneChaincode) appendIoTDeviceIDToIoTPlatform(iotPlatformUID string, deviceID string) (*model.IoTPlatform, error) {

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, iotPlatformUID)
	if err != nil {
		return nil, err
	}

	iotPlatform := model.IoTPlatform{}
	err = json.Unmarshal(iotPlatformJSON, &iotPlatform)
	if err != nil {
		return nil, err
	}

	i := sort.SearchStrings(iotPlatform.IoTDevices, deviceID)
	iotPlatform.IoTDevices = append(iotPlatform.IoTDevices[:i], append([]string{deviceID}, iotPlatform.IoTDevices[i:]...)...)

	iotPlatformJSON, _ = json.Marshal(iotPlatform)
	err = spc.putState(model.IoTPlatformType, iotPlatformUID, iotPlatformJSON)

	if err != nil {
		return nil, err
	}

	return &iotPlatform, nil
}

func (spc *SecurityPlaneChaincode) removeIoTDeviceIDInIoTPlatform(iotPlatformUID string, deviceID string) (*model.IoTPlatform, error) {

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, iotPlatformUID)
	if err != nil {
		return nil, err
	}

	iotPlatform := model.IoTPlatform{}
	err = json.Unmarshal(iotPlatformJSON, &iotPlatform)
	if err != nil {
		return nil, err
	}

	i := sort.Search(len(iotPlatform.IoTDevices), func(i int) bool { return deviceID == iotPlatform.IoTDevices[i] })
	if i >= len(iotPlatform.IoTDevices) || iotPlatform.IoTDevices[i] != deviceID {
		return nil, errors.New("not found, device ID")
	}

	iotPlatform.IoTDevices = model.RemoveItemByIndex(iotPlatform.IoTDevices, i)
	iotPlatformJSON, _ = json.Marshal(iotPlatform)
	err = spc.putState(model.IoTPlatformType, iotPlatformUID, iotPlatformJSON)

	if err != nil {
		return nil, err
	}

	return &iotPlatform, nil
}

func (spc *SecurityPlaneChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	_, args := stub.GetFunctionAndParameters()

	if len(args) != 0 {
		return shim.Error("Incorrect number of arguments. Expecting 0")
	}

	
	privateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	publicKey := &privateKey.PublicKey
	privateKeyBytes, publicKeyBytes := common.Encode(privateKey, publicKey)	
	serialNumberBytes := []byte(strconv.Itoa(0))

	fmt.Println(privateKey)
	fmt.Println(publicKey)
	fmt.Println(privateKeyBytes)
	fmt.Println(publicKeyBytes)	

	stub.PutState("CCPrivateKey", privateKeyBytes)
	stub.PutState("CCPublicKey", publicKeyBytes)
	stub.PutState("certSerialNumber", serialNumberBytes)

	return shim.Success(publicKeyBytes)
}

func (spc *SecurityPlaneChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()

	spc.function = function
	spc.args = args			

	/*
	fabricPrivateKey, _ := stub.GetState("CCPrivateKey")

	if (fabricPrivateKey == nil) {
		fmt.Println("Init CA private key")		
		privateKeyBytes, sk_err := ioutil.ReadFile("/etc/hyperledger/fabric/examples/example-private.pem")		
		//publicKeyBytes, pk_err := ioutil.ReadFile("/etc/hyperledger/fabric/examples/example-public.pem")		

		if sk_err != nil {
			panic(sk_err)		
		}
		
		bytes := common.ExtractPrivateKey(privateKeyBytes)

		fmt.Println(bytes)

		stub.PutState("CCPrivateKey", bytes)	
		fabricPrivateKey = bytes
	}
	*/
	spc.stub = stub		

	return spc.call()
}

func (spc *SecurityPlaneChaincode) enrollUser() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	id := args[0]
	pw := args[1]
	userPublicKey := args[2]

	isExist, err := spc.isExistData(model.UserType, id)

	if err != nil {
		return shim.Error(err.Error())
	}

	if isExist {
		return shim.Error("{\"state\": \"registered\"}")
	}

	fabricPrivateKey, _ := spc.stub.GetState("CCPrivateKey")
	fabricPublicKey, _ := spc.stub.GetState("CCPublicKey")
	serialNumberBytes, _ := spc.stub.GetState("certSerialNumber")
	caCRT, _ := spc.stub.GetState("CAcrt")

	fmt.Println(fabricPrivateKey)
	fmt.Println(fabricPublicKey)
	fmt.Println(userPublicKey)

	if (caCRT == nil) {
		tmp := common.CreateCertification2(fabricPrivateKey, fabricPublicKey, serialNumberBytes)
		spc.stub.PutState("CAcrt", tmp)
		caCRT = tmp
	}

	
	derBytes := common.CreateCertification(fabricPrivateKey, userPublicKey, serialNumberBytes, caCRT)	
	time, _ := spc.stub.GetTxTimestamp()
	

	user := model.User{
		KeyType:       "userType",
		UID:           id,
		Password:      common.SHA256Hash(pw),
		Certification: derBytes,
		CACertification: caCRT,
		IoTPlatforms:  []string{},
		CreatedAt:     time.GetSeconds(),
	}

	userJSON, err := json.Marshal(user)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.putState(model.UserType, id, userJSON)

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(userJSON)
}

func (spc *SecurityPlaneChaincode) enrollIoTPlatform() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	token := args[0]
	iotPlatformPublicKey := args[1]
	userID := args[2]
	platformID := args[3]

	uid := common.MakeUID(args[2:4])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	val, err := spc.getState(model.IoTPlatformType, uid)
	if err == nil && val != nil && len(val) > 0 {
		return shim.Error("already registered.")
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	fabricPrivateKey, _ := spc.stub.GetState("CCPrivateKey")	
	serialNumberBytes, _ := spc.stub.GetState("certSerialNumber")	
	caCRT, _ := spc.stub.GetState("CAcrt")
	
	derBytes := common.CreateCertification(fabricPrivateKey, iotPlatformPublicKey, serialNumberBytes, caCRT)	
	time, _ := spc.stub.GetTxTimestamp()

	iotPlatform := model.IoTPlatform{
		KeyType:         "iotPlatformType",
		UID:             uid,
		Certification:   derBytes,		
		Parent:          userID,
		IoTDevices:      []string{},
		CreatedAt:       time.GetSeconds(),
	}

	iotPlatformJSON, err := json.Marshal(iotPlatform)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.putState(model.IoTPlatformType, uid, iotPlatformJSON)
	if err != nil {
		return shim.Error(err.Error())
	}

	_, err = spc.appendIoTPlatformIDToUser(userID, platformID)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(iotPlatformJSON)
}

func (spc *SecurityPlaneChaincode) enrollIoTDevice() pb.Response {
	args := spc.args

	if len(args) != 9 {
		return shim.Error("Incorrect number of arguments. Expecting 9")
	}

	token := args[0]
	userID := args[1]
	platformID := args[2]
	deviceID := args[3]
	puid := common.MakeUID(args[1:3])
	uid := common.MakeUID(args[1:4])
	nickname := args[4]
	iotDevicePublicKey := args[5]
	location := args[6]
	description := args[7]
	resourceAddress := args[8]

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	val, err := spc.getState(model.IoTPlatformType, puid)
	if !(err == nil && val != nil && len(val) > 0) {
		return shim.Error("is not register : " + platformID)
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	val, err = spc.getState(model.IoTDeviceType, uid)
	if err == nil && val != nil && len(val) > 0 {
		return shim.Error("already registered.")
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	fabricPrivateKey, _ := spc.stub.GetState("CCPrivateKey")
	serialNumberBytes, _ := spc.stub.GetState("certSerialNumber")
	caCRT, _ := spc.stub.GetState("CAcrt")
	derBytes := common.CreateCertification(fabricPrivateKey, iotDevicePublicKey, serialNumberBytes, caCRT)

	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	time, err := spc.stub.GetTxTimestamp()

	iotDevice := model.IoTDevice{
		KeyType:         "iotDeviceType",
		UID:             uid,
		Nickname:        nickname,
		Certification:   derBytes,
		Parent:          platformID,
		Location:        location,
		Description:     description,
		ResourceAddress: resourceAddress,
		CreatedAt:       time.GetSeconds(),
	}

	iotDeviceJSON, err := json.Marshal(iotDevice)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.putState(model.IoTDeviceType, uid, iotDeviceJSON)
	if err != nil {
		return shim.Error(err.Error())
	}

	_, err = spc.appendIoTDeviceIDToIoTPlatform(puid, deviceID)

	return shim.Success(iotDeviceJSON)
}

func (spc *SecurityPlaneChaincode) reEnrollUser() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	verificationCode := args[0]
	id := args[1]
	pw := args[2]
	userPublicKey := args[3]

	isExist, err := spc.isExistData(model.UserType, id)

	if err != nil {
		return shim.Error(err.Error())
	}

	if !isExist {
		return shim.Error("is not exist : " + id)
	}

	if !spc.codeValidation(id, verificationCode) {
		return shim.Error("invalid verification_code")
	}

	fabricPrivateKey, _ := spc.stub.GetState("CCPrivateKey")
	serialNumberBytes, _ := spc.stub.GetState("certSerialNumber")
	userJSON, _ := spc.getState(model.UserType, id)
	caCRT, _ := spc.stub.GetState("CAcrt")
	derBytes := common.CreateCertification(fabricPrivateKey, userPublicKey, serialNumberBytes, caCRT)

	user := model.User{}

	err = json.Unmarshal(userJSON, &user)
	if err != nil {
		return shim.Error(err.Error())
	}

	if !bytes.Equal(common.SHA256Hash(pw), user.Password) {
		return shim.Error("incorrect password.")
	}

	user.Certification = derBytes
	user.CACertification = caCRT
	

	userJSON, err = json.Marshal(user)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.putState(model.UserType, id, userJSON)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(userJSON)
}

func (spc *SecurityPlaneChaincode) deployUserToken() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	id := args[0]
	pw := args[1]
	timestamp := args[2]
	signature := args[3]

	userJSON, err := spc.getState(model.UserType, id)
	if userJSON == nil {
		return shim.Error("is not registered.")
	}

	user := model.User{}
	err = json.Unmarshal(userJSON, &user)

	if err != nil {
		return shim.Error(err.Error())
	}

	if !bytes.Equal(common.SHA256Hash(pw), user.Password) {
		return shim.Error("invalid password")
	}

	if !model.VerifySignature(user.Certification, signature, common.SHA256Hash(id+pw+timestamp)) {
		return shim.Error("invalid signature")
	}

	err = spc.removeToken(id)
	if err != nil {
		return shim.Error(err.Error())
	}

	tokenBytes, err := spc.makeTokenAndPutState(id)

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"token\":\"" + hex.EncodeToString(tokenBytes) + "\"}"))
}

func (spc *SecurityPlaneChaincode) depolyIoTPlatformToken() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	// TODO : need to timestamp check
	signature := args[0]
	timestamp := args[1]
	userID := args[2]
	platformID := args[3]
	uid := common.MakeUID(args[2:])

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.removeToken(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotPlatform := model.IoTPlatform{}
	err = json.Unmarshal(iotPlatformJSON, &iotPlatform)

	if err != nil {
		return shim.Error(err.Error())
	}

	if model.VerifySignature(iotPlatform.Certification, signature, common.SHA256Hash(userID+platformID+timestamp)) {
		tokenBytes, err := spc.makeTokenAndPutState(uid)

		if err != nil {
			return shim.Error(err.Error())
		}

		return shim.Success([]byte("{\"token\":\"" + hex.EncodeToString(tokenBytes) + "\"}"))
	}

	return shim.Error("verify failed")
}

func (spc *SecurityPlaneChaincode) depolyIoTDeviceToken() pb.Response {
	args := spc.args

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	// TODO : need to timestamp check
	signature := args[0]
	timestamp := args[1]
	userID := args[2]
	platformID := args[3]
	deviceID := args[4]
	uid := common.MakeUID(args[2:])

	iotDeviceJSON, err := spc.getState(model.IoTDeviceType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.removeToken(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotDevice := model.IoTPlatform{}
	err = json.Unmarshal(iotDeviceJSON, &iotDevice)

	if err != nil {
		return shim.Error(err.Error())
	}

	if model.VerifySignature(iotDevice.Certification, signature, common.SHA256Hash(userID+platformID+deviceID+timestamp)) {
		tokenBytes, err := spc.makeTokenAndPutState(uid)

		if err != nil {
			return shim.Error(err.Error())
		}

		return shim.Success([]byte("{\"token\":\"" + hex.EncodeToString(tokenBytes) + "\"}"))
	}

	return shim.Error("verify failed")
}

func (spc *SecurityPlaneChaincode) deployCodeInUser() pb.Response {
	args := spc.args

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	id := args[0]

	verificationCodeBytes, err := spc.makeCodeAndPutState(id)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"verification_code\":\"" + hex.EncodeToString(verificationCodeBytes) + `"}`))
}

func (spc *SecurityPlaneChaincode) deployCodeInIoTPlatform() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	_, err := spc.makeCodeAndPutState(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\":\"success\"}"))
}

func (spc *SecurityPlaneChaincode) deployCodeInIoTDevice() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	_, err := spc.makeCodeAndPutState(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\":\"success\"}"))
}

func (spc *SecurityPlaneChaincode) removeIoTPlatform() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	userToken := args[0]
	userID := args[1]
	platformID := args[2]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, userToken) {
		return shim.Error("token is not valid")
	}

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}
	if iotPlatformJSON == nil {
		return shim.Error("invalid PlatformID : " + platformID)
	}

	iotPlatform := model.IoTPlatform{}
	err = json.Unmarshal(iotPlatformJSON, &iotPlatform)

	if len(iotPlatform.IoTDevices) > 0 {
		return shim.Error("The device exists, Please delete all devices first.")
	}

	err = spc.delState(model.IoTPlatformType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.removeToken(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	user, err := spc.removeIoTPlatformIDInUser(userID, platformID)
	if err != nil {
		return shim.Error(err.Error())
	}

	userJSON, _ := json.Marshal(user)

	return shim.Success(userJSON)
}

func (spc *SecurityPlaneChaincode) removeIoTDevice() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	userToken := args[0]
	userID := args[1]
	platformID := args[2]
	deviceID := args[3]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, userToken) {
		return shim.Error("token is not valid")
	}

	isExist, err := spc.isExistData(model.IoTDeviceType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !isExist {
		return shim.Error("DeviceID is wrong")
	}

	err = spc.delState(model.IoTDeviceType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = spc.removeToken(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotPlatform, err := spc.removeIoTDeviceIDInIoTPlatform(userID+"_"+platformID, deviceID)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotPlatformJSON, _ := json.Marshal(iotPlatform)

	return shim.Success(iotPlatformJSON)
}

func (spc *SecurityPlaneChaincode) removeUserToken() pb.Response {
	args := spc.args

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	token := args[0]
	uid := args[1]

	if !spc.tokenValidation(uid, token) {
		return shim.Error("token is not valid")
	}

	err := spc.removeToken(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\": \"success\"}"))
}

func (spc *SecurityPlaneChaincode) removeIoTPlatformToken() pb.Response {
	args := spc.args

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	signature := args[0]
	timestamp := args[1]
	platformToken := args[2]
	uid := common.MakeUID(args[3:])

	if !spc.tokenValidation(uid, platformToken) {
		return shim.Error("token is not valid")
	}

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotPlatform := model.IoTPlatform{}
	err = json.Unmarshal(iotPlatformJSON, &iotPlatform)

	if !model.VerifySignature(iotPlatform.Certification, signature, common.SHA256Hash(platformToken+timestamp)) {
		return shim.Error("signature is wrong")
	}

	err = spc.removeToken(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\": \"success\"}"))
}

func (spc *SecurityPlaneChaincode) removeIoTDeviceToken() pb.Response {
	args := spc.args

	if len(args) != 6 {
		return shim.Error("Incorrect number of arguments. Expecting 6")
	}

	signature := args[0]
	timestamp := args[1]
	deviceToken := args[2]
	uid := common.MakeUID(args[3:])

	if !spc.tokenValidation(uid, deviceToken) {
		return shim.Error("token is not valid")
	}

	iotDeviceJSON, err := spc.getState(model.IoTDeviceType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotDevice := model.IoTDevice{}
	err = json.Unmarshal(iotDeviceJSON, &iotDevice)
	if err != nil {
		return shim.Error(err.Error())
	}

	if !model.VerifySignature(iotDevice.Certification, signature, common.SHA256Hash(deviceToken+timestamp)) {
		return shim.Error("signature is wrong")
	}

	err = spc.removeToken(uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\": \"success\"}"))
}

func (spc *SecurityPlaneChaincode) removeUserCode() pb.Response {
	args := spc.args

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	uid := args[0]

	err := spc.delState(model.VerificationCodeType, uid)

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\": \"success\"}"))
}

func (spc *SecurityPlaneChaincode) removeIoTPlatformCode() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	err := spc.delState(model.VerificationCodeType, uid)

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\": \"success\"}"))
}

func (spc *SecurityPlaneChaincode) removeIoTDeviceCode() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	err := spc.delState(model.VerificationCodeType, uid)

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte("{\"state\": \"success\"}"))
}

func (spc *SecurityPlaneChaincode) isExistUser() pb.Response {
	args := spc.args

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	uid := args[0]

	isExist, err := spc.isExistData(model.UserType, uid)

	if err != nil {
		return shim.Error(err.Error())
	}

	if !isExist {
		return shim.Success([]byte("{\"is_exist\":false}"))
	}

	return shim.Success([]byte("{\"is_exist\":true}"))
}

func (spc *SecurityPlaneChaincode) isExistIoTPlatform() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	isExist, err := spc.isExistData(model.IoTPlatformType, uid)

	if err != nil {
		return shim.Error(err.Error())
	}

	if !isExist {
		return shim.Success([]byte("{\"is_exist\":false}"))
	}

	return shim.Success([]byte("{\"is_exist\":true}"))
}

func (spc *SecurityPlaneChaincode) isExistIoTDevice() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	isExist, err := spc.isExistData(model.IoTDeviceType, uid)

	if err != nil {
		return shim.Error(err.Error())
	}

	if !isExist {
		return shim.Success([]byte("{\"is_exist\":false}"))
	}

	return shim.Success([]byte("{\"is_exist\":true}"))
}

func (spc *SecurityPlaneChaincode) isValidUserToken() pb.Response {
	args := spc.args

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	token := args[0]
	uid := args[1]

	if !spc.tokenValidation(uid, token) {
		return shim.Success([]byte("{\"is_valid\":false}"))
	}

	return shim.Success([]byte("{\"is_valid\":true}"))

}

func (spc *SecurityPlaneChaincode) isValidIoTPlatformToken() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	token := args[0]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(uid, token) {
		return shim.Success([]byte("{\"is_valid\":false}"))
	}

	return shim.Success([]byte("{\"is_valid\":true}"))

}

func (spc *SecurityPlaneChaincode) isValidIoTDeviceToken() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	token := args[0]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(uid, token) {
		return shim.Success([]byte("{\"is_valid\":false}"))
	}

	return shim.Success([]byte("{\"is_valid\":true}"))

}

func (spc *SecurityPlaneChaincode) isValidUserCode() pb.Response {
	args := spc.args

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	code := args[0]
	uid := args[1]

	if !spc.codeValidation(uid, code) {
		return shim.Success([]byte("{\"is_valid\":false}"))
	}

	return shim.Success([]byte("{\"is_valid\":true}"))
}

func (spc *SecurityPlaneChaincode) isValidIoTPlatformCode() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	token := args[0]
	code := args[1]
	userID := args[2]
	uid := common.MakeUID(args[2:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	if !spc.codeValidation(uid, code) {
		return shim.Success([]byte("{\"is_valid\":false}"))
	}

	return shim.Success([]byte("{\"is_valid\":true}"))
}

func (spc *SecurityPlaneChaincode) isValidIoTDeviceCode() pb.Response {
	args := spc.args

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	token := args[0]
	code := args[1]
	userID := args[2]
	uid := common.MakeUID(args[2:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	if !spc.codeValidation(uid, code) {
		return shim.Success([]byte("{\"is_valid\":false}"))
	}

	return shim.Success([]byte("{\"is_valid\":true}"))
}

func (spc *SecurityPlaneChaincode) isValidIoTPlatformSignature() pb.Response {
	args := spc.args

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	token := args[0]
	signature := args[1]
	msg := args[2]
	// timestamp := args[3]
	userID := args[3]
	uid := common.MakeUID(args[3:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotPlatform := model.IoTPlatform{}
	err = json.Unmarshal(iotPlatformJSON, &iotPlatform)

	if err != nil {
		return shim.Error(err.Error())
	}

	if !model.VerifySignature(iotPlatform.Certification, signature, common.SHA256Hash(msg)) {
		return shim.Success([]byte("{\"is_valid\":false}"))
	}

	return shim.Success([]byte("{\"is_valid\":true}"))
}

func (spc *SecurityPlaneChaincode) isValidIoTDeviceSignature() pb.Response {
	args := spc.args

	if len(args) != 6 {
		return shim.Error("Incorrect number of arguments. Expecting 6")
	}

	token := args[0]
	signature := args[1]
	msg := args[2]
	// timestamp := args[3]
	userID := args[3]
	uid := common.MakeUID(args[3:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	iotDeviceJSON, err := spc.getState(model.IoTDeviceType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotDevice := &model.IoTDevice{}
	err = json.Unmarshal(iotDeviceJSON, &iotDevice)

	if err != nil {
		return shim.Error(err.Error())
	}

	if model.VerifySignature(iotDevice.Certification, signature, common.SHA256Hash(msg)) {
		return shim.Success([]byte("{\"is_valid\": true}"))
	}
	return shim.Success([]byte("{\"is_valid\": false}"))
}

func (spc *SecurityPlaneChaincode) getUserTokenByID() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	id := args[0]
	pw := args[1]
	timestamp := args[2]
	signature := args[3]

	userJSON, err := spc.getState(model.UserType, id)
	if userJSON == nil {
		return shim.Error("is not registered.")
	}

	user := model.User{}
	err = json.Unmarshal(userJSON, &user)

	if err != nil {
		return shim.Error(err.Error())
	}

	if !model.VerifySignature(user.Certification, signature, common.SHA256Hash(id+pw+timestamp)) {
		return shim.Error("invalid signature")
	}

	if !bytes.Equal(common.SHA256Hash(pw), user.Password) {
		return shim.Error("incorrect password.")
	}

	tokenBytes, err := spc.getState(model.TokenType, id)
	if err != nil {
		return shim.Error(err.Error())
	}
	if tokenBytes == nil {
		return shim.Error("No token was issued.")
	}

	return shim.Success([]byte("{\"token\":\"" + hex.EncodeToString(tokenBytes) + "\"}"))
}

//TODO : random number ... timestamp... etc..
// sign(id + value) structure
func (spc *SecurityPlaneChaincode) getIoTPlatformTokenWithSignature() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	signature := args[0]
	timestamp := args[1]
	userID := args[2]
	platformID := args[3]
	uid := common.MakeUID(args[2:])

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotPlatform := model.IoTPlatform{}
	err = json.Unmarshal(iotPlatformJSON, &iotPlatform)

	if err != nil {
		return shim.Error(err.Error())
	}

	if model.VerifySignature(iotPlatform.Certification, signature, common.SHA256Hash(userID+platformID+timestamp)) {
		tokenBytes, err := spc.getState(model.TokenType, uid)
		if err != nil {
			return shim.Error(err.Error())
		}

		if tokenBytes == nil {
			return shim.Error("token was not issued.")
		}

		return shim.Success([]byte("{\"token\":\"" + hex.EncodeToString(tokenBytes) + "\"}"))
	}

	return shim.Error("verify failed")
}

func (spc *SecurityPlaneChaincode) getIoTDeviceTokenWithSignature() pb.Response {
	args := spc.args

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	signature := args[0]
	timestamp := args[1]
	userID := args[2]
	platformID := args[3]
	deviceID := args[4]
	uid := common.MakeUID(args[2:])

	iotDeviceJSON, err := spc.getState(model.IoTDeviceType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	iotDevice := model.IoTDevice{}
	err = json.Unmarshal(iotDeviceJSON, &iotDevice)

	if err != nil {
		return shim.Error(err.Error())
	}

	if model.VerifySignature(iotDevice.Certification, signature, common.SHA256Hash(userID+platformID+deviceID+timestamp)) {
		tokenBytes, err := spc.getState(model.TokenType, uid)
		if err != nil {
			return shim.Error(err.Error())
		}

		if tokenBytes == nil {
			return shim.Error("No token was issued.")
		}

		return shim.Success([]byte("{\"token\":\"" + hex.EncodeToString(tokenBytes) + "\"}"))
	}

	return shim.Error("verify failed")
}

func (spc *SecurityPlaneChaincode) getUserInformation() pb.Response {
	args := spc.args

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	token := args[0]
	userID := args[1]

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	userJSON, err := spc.getState(model.UserType, userID)
	if err != nil {
		return shim.Error(err.Error())
	}

	if userJSON == nil {
		return shim.Error("is not registered.")
	}

	return shim.Success(userJSON)
}

func (spc *SecurityPlaneChaincode) getIoTPlatformInformation() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	iotPlatformJSON, err := spc.getState(model.IoTPlatformType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	if iotPlatformJSON == nil {
		return shim.Error("is not registered.")
	}

	return shim.Success(iotPlatformJSON)
}

func (spc *SecurityPlaneChaincode) getIoTDeviceInformation() pb.Response {
	args := spc.args

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	token := args[0]
	userID := args[1]
	uid := common.MakeUID(args[1:])

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	iotDeviceJSON, err := spc.getState(model.IoTDeviceType, uid)
	if err != nil {
		return shim.Error(err.Error())
	}

	if iotDeviceJSON == nil {
		return shim.Error("is not registered.")
	}

	return shim.Success(iotDeviceJSON)
}

func (spc *SecurityPlaneChaincode) getIoTPlatformsByUserID() pb.Response {
	args := spc.args

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	token := args[0]
	userID := args[1]

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	queryString := fmt.Sprintf("{\"selector\":{\"parent\":\"%s\"}}", userID)

	resultsIterator, err := spc.stub.GetQueryResult(queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("{\"iot_platforms\":[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]}")
	return shim.Success(buffer.Bytes())
}

func (spc *SecurityPlaneChaincode) getIoTDevicesByIoTPlatformID() pb.Response {
	args := spc.args

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	token := args[0]
	userID := args[1]
	platformID := args[2]

	if !spc.tokenValidation(userID, token) {
		return shim.Error("token is not valid")
	}

	queryString := fmt.Sprintf("{\"selector\":{\"parent\":\"%s\"}}", platformID)

	resultsIterator, err := spc.stub.GetQueryResult(queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("{\"iot_devices\":[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]}")
	return shim.Success(buffer.Bytes())
}

func main() {
	err := shim.Start(new(SecurityPlaneChaincode))
	if err != nil {
		fmt.Printf("Error starting User chaincode: %s", err)
	}
}
