# cd front && forever start node_modules/@angular/cli/bin/ng serve

sdir="$( cd "$(dirname "$0")" ; pwd -P )"
sname=sp_server



function server_init
{
	#source $sdir/../common/logger.sh
	source $NVM_DIR/nvm.sh
	nvm install 8.16.0

	set -a
	. $sdir/.env
	set +a
}

function server_all
{
    server_clean
    server_build
	server_start
}


function server_build
{
	info "Build for server"

	nvm install 8.16.0
	cd $sdir
	npm install
	cd $sdir/sdk
	npm install
}

function server_stop
{
    info "Server stop"
	pm2 stop $sname
}

function server_start
{
	info "Server start"
	#cd $sdir && pm2 start "npm start" --name $sname
	cd $sdir && npm start
}

function server_clean
{
	info "Clean up server"
	pm2 delete $sname
    rm -rf $sdir/node_modules
    rm -rf $sdir/sdk/node_modules
    rm -rf $sdir/sdk/wallet
}

function main
{
	server_init
	case $1 in
		all | build | start | stop | clean )
			cmd=$1
			shift
			server_$cmd $@
		;;
		*)
			error "Not supported command"
			exit 1
		;;
	esac
}

main $@
