var fs =  require("fs");
var path = require("path");
var express = require('express');
var fabricChaincode = require("../sdk/chaincode");
var router = express.Router();
var emailjs = require('emailjs');
const configPath = path.resolve(__dirname, '../config/config.json');
const configJSON = fs.readFileSync(configPath, 'utf8');
const config = JSON.parse(configJSON);

var email_server = emailjs.server.connect(config.email);


// 	token := args[0]
// 	userID := args[1]
// 	platformID := args[2 
// 	deviceID := args[3]
// 	nickname := args[4]
// 	publicKey := args[5]
// 	location := args[6]
// 	description := args[7]
// 	resourceAddress := args[8]
router.post('/enroll', function(req, res, next) {
    console.log('[ api/v1/iot_device/enroll POST ]');
    var args = `{"Args":["enrollIoTDevice","${req.body.token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}","${req.body.nickname}","${req.body.public_key}","${req.body.location}","${req.body.description}","${req.body.resourceAddress}"]}`;
    console.log(args);
    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.status(409).send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/info', function(req, res, next) {
    console.log('[ api/v1/iot_device/info POST ]');
    var args =  `{"Args":["getIoTDeviceInformation","${req.body.token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`;
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/is_exist', function(req, res, next) {
    console.log('[ api/v1/iot_device/is_exist POST ]');
    var args = `{"Args":["isExistIoTDevice","${req.body.token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/remove', function(req, res, next) {
    console.log('[ api/v1/iot_device/remove POST ]');
    var args = `{"Args":["removeIoTDevice","${req.body.token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`
    console.log(args);
    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/token/deploy', function(req, res, next) {
    console.log('[ api/v1/iot_device/token/deploy POST ]');
    // signature = sign( SHA256(user_id + platform_id + device_id + timestamp), base64 );
    var args = `{"Args":["depolyIoTDeviceToken","${req.body.signature}","${req.body.timestamp}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/get', function(req, res, next) {
    console.log('[ api/v1/iot_device/token/get POST ]');
    // signature = sign( SHA256(user_id + platform_id + device_id + timestamp), base64 );
    var args = `{"Args":["getIoTDeviceTokenWithSignature","${req.body.signature}","${req.body.timestamp}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`
    console.log(args);

    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/is_valid', function(req, res, next) {
    console.log('[ api/v1/iot_platform/token/is_valid POST ]');
    var args =  `{"Args":["isValidIoTDeviceToken","${req.body.token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`;
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/remove', function(req, res, next) {
    console.log('[ api/v1/iot_device/token/remove POST ]');
    // signature = SHA256(user.id+platform.id+device.id+timestamp) base64;
    var args = `{"Args":["removeIoTDeviceToken","${req.body.signature}","${req.body.timestamp}","${req.body.device_token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/verification_code/deploy', function(req, res, next) {
    console.log('[ api/v1/iot_device/verificode_code/deploy ]');

    if (req.body.user_id != null) {
        var args =  `{"Args":["deployCodeInIoTDevice","${req.body.token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`;

        fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
            if (err != null) {
                res.send(err);
                return;
            }

            email_server.send({
                text: 'Your verification code is ' + data.verification_code ,
                from: 'seongjinlee@smartm2m.co.kr',
                to: req.body.user_id,
                subject: '[Security Plane] Verification code for IoT Device'
            }, function(err, message) { console.log(err || message);});

            res.send('{"state":"success"}');
        });
    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
        res.send(req.body);
    }
});

router.post('/verification_code/remove', function(req, res, next) {
    console.log('[ api/v1/iot_device/verification_code/remove POST ]');
    var args = `{"Args":["removeIoTDeviceCode","${req.body.token}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});


router.post('/verification_code/is_valid', function(req, res, next) {
    console.log('[ api/v1/iot_device/verification_code/is_valid ]');

    if (req.body.user_id != null) {
        var args =  `{"Args":["isValidIoTDeviceCode","${req.body.verification_code}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`;

        fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
            if (err != null) {
                res.send(err);
                return;
            }
            console.log(data.toString());
            res.send(data.toString());
        });

    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
        res.send(req.body);
    }
});

router.post('/signature/is_valid', function(req, res, next) {
    console.log('[ api/v1/iot_device/signature/is_valid POST ]');
    // signature = SHA256(msg) base64;
    var args = `{"Args":["isValidIoTDeviceSignature","${req.body.token}","${req.body.signature}","${req.body.msg}","${req.body.user_id}","${req.body.platform_id}","${req.body.device_id}"]}`
    console.log(args);

    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

module.exports = router;
