var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var serv = require('../../server');

router.get('/user/info', function(req, res, next) {
    console.log('[ api/v1/db/trusthingzdb GET ]');
    console.log(req.url);
    if (req.body.register != null) {
        serv.db.find({ _id: req.body.register.email }).toArray((err, items) => {
            console.log("[ Find result ]");
            console.log(items);
        });
    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
    }
    res.send(req.body);
});

var BCRYPT_SALT_ROUNDS = 12;
router.post('/user/register', function(req, res, next) {
    var resp = {
        'db': ''
    };
    console.log('[ api/v1/db/trusthingzdb POST ]');
    if (req.body.register != null) {
        bcrypt.hash(req.body.register.password, BCRYPT_SALT_ROUNDS)
            .then(function(hashedPassword) {
                serv.db.insertOne({
                    _id: req.body.register.email,
                    password: hashedPassword,
                    type: 'user',
                    confirm: false,
                    confirm_code: ''
                });
            })
            .catch(function(error) {
                console.log('[ Error! ]');
                console.log(error)
            })
            resp['db'] = 'success';
    } else if (req.body.confirm_req != null) {
        serv.db.update({ _id: req.body.confirm_req.email }, { $set: { confirm_code: req.body.confirm_req.confirm_code }});
        resp['db'] = 'success';
    } else if (req.body.confirm != null) {
        console.log('Confirm Email!');
        serv.db.find({ _id: req.body.confirm.email }).toArray((err, items) => {
            if (req.body.confirm.confirm_code == items[0].confirm_code) {
                serv.db.update({ _id: req.body.confirm.email }, { $set: { confirm: true }});
                resp['db'] = 'success';
            } else {
                resp['db'] = 'fail! Confirm code not match';
            }
        });
    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
        resp['db'] = 'fail';
    }
    res.send(resp);
});

router.delete('/trusthingzdb', function(req, res, next){
    if (req.body.user != null) {
        serv.db.deleteOne( {_id: req.body.user.email});
    }
    res.send();
});

module.exports = router;