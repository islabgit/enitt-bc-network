var fs =  require("fs");
var path = require("path");
var express = require('express');
var fabricChaincode = require("../sdk/chaincode");
var router = express.Router();
var emailjs = require('emailjs');
const configPath = path.resolve(__dirname, '../config/config.json');
const configJSON = fs.readFileSync(configPath, 'utf8');
const config = JSON.parse(configJSON);

var email_server = emailjs.server.connect({
    user: "pnu.myislab@gmail.com",
    password: "l1954tp!",
    host: "smtp.gmail.com",
    port: 587,
    tls: true
});

// 	userID := args[0]
// 	password := args[1]
router.post('/enroll', function(req, res, next) {
    console.log('[ api/v1/user/enroll POST ]');
    var args = `{"Args":["enrollUser","${req.body.user_id}","${req.body.password}","${req.body.public_key}"]}`
    console.log(args);
    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.status(409).send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/getUserInfoFromEth', function(req, res, next) {
    console.log('[ api/v1/user/getUserInfoFromEth POST ]');
    res.send(result.toString());
});

router.post('/re_enroll', function(req, res, next) {
    console.log('[ api/v1/user/re_enroll POST ]');
    var args = `{"Args":["reEnrollUser","${req.body.verification_code}","${req.body.user_id}","${req.body.password}","${req.body.public_key}"]}`
    console.log(args);
    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.status(409).send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/info', function(req, res, next) {
    console.log('[ api/v1/user/info POST ]');
    var args =  `{"Args":["getUserInformation","${req.body.token}","${req.body.user_id}"]}`;
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/info/platforms', function(req, res, next) {
    console.log('[ api/v1/user/info/platforms POST ]');
    var args =  `{"Args":["getIoTPlatformsByUserID","${req.body.token}","${req.body.user_id}"]}`;
    console.log(args);
    
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/is_exist', function(req, res, next) {
    console.log('[ api/v1/user/is_exist POST ]');
    var args = `{"Args":["isExistUser","${req.body.user_id}"]}`
    console.log(args);
    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.send(err);
            return;
        }
        res.send(data.toString());
    });
});


router.post('/token/deploy', function(req, res, next) {
    console.log('[ api/v1/user/token/deploy POST ]');
    var args = `{"Args":["deployUserToken","${req.body.user_id}","${req.body.password}","${req.body.timestamp}","${req.body.signature}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/get', function(req, res, next) {
    console.log('[ api/v1/user/token/get POST ]');
    var args = `{"Args":["getUserTokenByID","${req.body.user_id}","${req.body.password}","${req.body.timestamp}","${req.body.signature}"]}`
    console.log(args);

    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/is_valid', function(req, res, next) {
    console.log('[ api/v1/user/token/is_valid POST ]');
    var args =  `{"Args":["isValidUserToken","${req.body.token}","${req.body.user_id}"]}`;
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/remove', function(req, res, next) {
    console.log('[ api/v1/user/token/remove POST ]');
    var args = `{"Args":["removeUserToken","${req.body.token}","${req.body.user_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/verification_code/deploy', function(req, res, next) {
    console.log('[ api/v1/user/verification_code/deploy ]');

    if (req.body.user_id != null) {
        var args =  `{"Args":["deployCodeInUser","${req.body.user_id}"]}`;

        fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
            if (err != null) {
                console.log('err test!');
                res.send(err);
                return;
            }
            const result = JSON.parse(data);

            email_server.send({
                text: 'Your verification code is ' + result.verification_code ,
                from: 'seongjinlee@smartm2m.co.kr',
                to: req.body.user_id,
                subject: '[Security Plane] Verification code for User'
            }, function(err, message) { console.log(err || message);});


            res.send('{"state":"success"}');
        });
    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
        res.send(req.body);
    }
});

router.post('/verification_code/remove', function(req, res, next) {
    console.log('[ api/v1/user/verification_code/remove POST ]');
    var args = `{"Args":["removeUserCode", "${req.body.user_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/verification_code/is_valid', function(req, res, next) {
    console.log('[ api/v1/user/verification_code/is_valid ]');

    if (req.body.user_id != null) {
        var args =  `{"Args":["isValidUserCode","${req.body.verification_code}","${req.body.user_id}"]}`;

        fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
            if (err != null) {
                res.send(err);
                return;
            }
            console.log(data.toString());
            res.send(data.toString());
        });

    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
        res.send(req.body);
    }
});

module.exports = router;
