var fs =  require("fs");
var path = require("path");
var express = require('express');
var fabricChaincode = require("../sdk/chaincode");
var router = express.Router();
var emailjs = require('emailjs');
const configPath = path.resolve(__dirname, '../config/config.json');
const configJSON = fs.readFileSync(configPath, 'utf8');
const config = JSON.parse(configJSON);

var email_server = emailjs.server.connect(config.email);


// 	token := args[0]
// 	userID := args[1]
// 	platformID := args[2]
// 	publicKey := args[3]
router.post('/enroll', function(req, res, next) {
    console.log('[ api/v1/iot_platform/enroll POST ]');
    var args = `{"Args":["enrollIoTPlatform","${req.body.token}","${req.body.public_key}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.status(409).send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/info', function(req, res, next) {
    console.log('[ api/v1/iot_platform/info POST ]');
    var args =  `{"Args":["getIoTPlatformInformation","${req.body.token}","${req.body.user_id}","${req.body.platform_id}"]}`;
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/info/devices', function(req, res, next) {
    console.log('[ api/v1/iot_platform/info/devices POST ]');
    var args =  `{"Args":["getIoTDevicesByIoTPlatformID","${req.body.token}","${req.body.user_id}","${req.body.platform_id}"]}`;
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/remove', async function(req, res, next) {
    console.log('[ api/v1/iot_platform/remove POST ]');

    var args = `{"Args":["removeIoTPlatform","${req.body.token}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);
    await fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.status(409).send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/is_exist', function(req, res, next) {
    console.log('[ api/v1/iot_platform/is_exist POST ]');
    var args = `{"Args":["isExistIoTPlatform","${req.body.token}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);
    chaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err){
            res.send(err);
            return;
        }
        res.send(data.toString());
    });
});

router.post('/token/deploy', function(req, res, next) {
    console.log('[ api/v1/iot_platform/token/deploy POST ]');
    // signature = sign( SHA256(user_id + platform_id + timestamp), base64 );
    var args = `{"Args":["depolyIoTPlatformToken","${req.body.signature}","${req.body.timestamp}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/get', function(req, res, next) {
    console.log('[ api/v1/iot_platform/token/get POST ]');
    // signature = sign( SHA256(user_id + platform_id + timestamp), base64 );
    var args = `{"Args":["getIoTPlatformTokenWithSignature","${req.body.signature}","${req.body.timestamp}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);

    chaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/is_valid', function(req, res, next) {
    console.log('[ api/v1/iot_platform/token/is_valid POST ]');
    var args =  `{"Args":["isValidIoTPlatformToken","${req.body.token}","${req.body.user_id}","${req.body.platform_id}"]}`;
    console.log(args);
    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/token/remove', function(req, res, next) {
    console.log('[ api/v1/iot_platform/token/remove POST ]');
    // signature = SHA256(user.id+platform.id+timestamp) base64;
    var args = `{"Args":["removeIoTPlatformToken","${req.body.signature}","${req.body.timestamp}","${req.body.platform_token}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);

    chaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/verification_code/deploy', function(req, res, next) {
    console.log('[ api/v1/iot_platform/verificode_code/deploy ]');

    if (req.body.user_id != null) {
        var args =  `{"Args":["deployCodeInIoTPlatform","${req.body.token}","${req.body.user_id}","${req.body.platform_id}"]}`;
        fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
            if (err != null) {
                res.send(err);
                return;
            }

            email_server.send({
                text: 'Your verification code is ' + data.verification_code ,
                from: 'seongjinlee@smartm2m.co.kr',
                to: req.body.user_id,
                subject: '[Security Plane] Verification code for IoT Platform'
            }, function(err, message) { console.log(err || message);});

            res.send('{"state":"success"}');
        });
    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
        res.send(req.body);
    }
});

router.post('/verification_code/is_valid', function(req, res, next) {
    console.log('[ api/v1/iot_platform/verification_code/is_valid ]');

    if (req.body.user_id != null) {
        var args =  `{"Args":["isValidCodeInIoTPlatform","${req.body.verification_code}","${req.body.user_id}","${req.body.platform_id}"]}`;

        fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
            if (err != null) {
                res.send(err);
                return;
            }
            console.log(data.toString());
            res.send(data.toString());
        });

    } else {
        console.log('[ Function find error! ]');
        console.log(' Check below your request!');
        console.log(req.body);
        res.send(req.body);
    }
});

router.post('/verification_code/remove', function(req, res, next) {
    console.log('[ api/v1/iot_platform/verification_code/remove POST ]');
    var args = `{"Args":["removeIoTPlatformCode","${req.body.token}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);

    fabricChaincode.invoke([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

router.post('/signature/is_valid', function(req, res, next) {
    console.log('[ api/v1/iot_platform/signature/is_valid POST ]');
    // signature = SHA256(msg) base64;
    var args = `{"Args":["isValidIoTPlatformSignature","${req.body.token}","${req.body.signature}","${req.body.msg}","${req.body.user_id}","${req.body.platform_id}"]}`
    console.log(args);

    fabricChaincode.query([SDKID, ChannelName, ChaincodeName, args], (err, data) => {
        if (err != null) {
            res.send(err);
            return;
        }
        console.log(data.toString());
        res.send(data.toString());
    });
});

module.exports = router;
