// server/server.js
var express = require("express");
var app = express();
var fs = require("fs");
var path = require("path");
var bodyParser = require("body-parser");
var DatabaseRouter = require('./api/backend_db/db');
var FabricSDKEnroll = require("./sdk/enroll");
var FabricUserRouter = require('./api/user');
var FabricIoTPlatformRouter = require('./api/iot_platform');
var FabricIoTDeviceRouter = require('./api/iot_device');
const dotenv = require('dotenv');
const result = dotenv.config();

const configPath = path.resolve(__dirname, 'config/config.json');
const configJSON = fs.readFileSync(configPath, 'utf8');
const config = JSON.parse(configJSON);

global.SDKID = config.fabric.sdk_id;
global.ChannelName = config.fabric.channel_name;
global.ChaincodeName = config.fabric.chaincode_name;
global.buildPath = path.resolve(__dirname, '../ethereum/build');

function init() {
  FabricSDKEnroll.admin([SDKID, config.fabric.sdk_password, config.fabric.msp_id, config.fabric.ca_url]);
}

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "content-type");
  next();
});

init();

app.use('/api/v1/user', FabricUserRouter);
app.use('/api/v1/iot_platform', FabricIoTPlatformRouter);
app.use('/api/v1/iot_device', FabricIoTDeviceRouter);

// Angular
app.get("*", function(req, res) {
  //2
  var indexFile = path.resolve(
    __dirname,
    "../dist/miletus-server-ng/index.html"
  );
  res.sendFile(indexFile);
});

// Server
var port = result.parsed.API_SERVER_PORT;
app.listen(port, function() {
  console.log("listening on port:" + port);
});
