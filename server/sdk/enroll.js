'use strict';

const FabricCAServices = require('fabric-ca-client');
const { FileSystemWallet, X509WalletMixin, Gateway } = require('fabric-network');
const fs = require('fs');
const path = require('path');
const express = require('express');

const ccpPath = path.resolve(__dirname, 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);

const walletPath = path.join(__dirname, 'wallet');
const wallet = new FileSystemWallet(walletPath);

// adminID
// adminPassword
// orgMspID
// caURL

module.exports.admin = async (args) => {
    try {
        let adminID         = args[0];
        let adminPassword   = args[1];
        let orgMspID        = args[2];
        let caURL           = args[3];
        const caInfo = ccp.certificateAuthorities[caURL];
        console.log("target path: " + caInfo.tlsCACerts.path);
        const caTLSCACerts = fs.readFileSync(caInfo.tlsCACerts.path);
        const ca = new FabricCAServices(caInfo.url, { trustedRoots: caTLSCACerts, verify: false }, caInfo.caName);
        const adminExists = await wallet.exists(adminID);
        if(adminExists){
            console.log(`An identity for the ${adminID} already exists in the wallet`);
            return;
        }

        // Enroll the admin user, and import the new identity into the wallet.
        const enrollment = await ca.enroll({ enrollmentID: adminID, enrollmentSecret: adminPassword });
        const identity = X509WalletMixin.createIdentity(orgMspID, enrollment.certificate, enrollment.key.toBytes());

        await wallet.import(adminID, identity);
        console.log(`Successfully enrolled ${adminID} and imported it into the wallet`);
    } catch (error) {
        console.error(`Failed to enroll : ${error}`);
    }
}

// userID
// orgMspID
module.exports.user = async (args) => {
    try {
        let userID   = args[0];
        let orgMspID = args[1];

        const userExists = await wallet.exists(userID);
        if (userExists) {
            console.log(`An identity for the user ${userID} already exists in the wallet`);
            return;
        }

        // Check to see if we've already enrolled the admin user.
        const adminExists = await wallet.exists('admin');
        if (!adminExists) {
            console.log('An identity for the admin user "admin" does not exist in the wallet');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: 'admin', discovery: { enabled: true, asLocalhost: true } });

        // Get the CA client object from the gateway for interacting with the CA.
        const ca = gateway.getClient().getCertificateAuthority();
        const adminIdentity = gateway.getCurrentIdentity();

        // Register the user, enroll the user, and import the new identity into the wallet.
        const secret = await ca.register({ affiliation: '', enrollmentID: userID, role: 'client' }, adminIdentity);
        const enrollment = await ca.enroll({ enrollmentID: userID, enrollmentSecret: secret });
        const userIdentity = X509WalletMixin.createIdentity(orgMspID, enrollment.certificate, enrollment.key.toBytes());
        await wallet.import(userID, userIdentity);
        console.log(`Successfully registered and enrolled user : ${userID} and imported it into the wallet`);
    } catch (error) {
        console.error(`Failed to enroll ${userID}: ${error}`);
    }
}