'use strict';

const { FileSystemWallet, Gateway } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const ccpPath = path.resolve(__dirname, 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);

module.exports.query = async (args, callback) => {
    try {
        let userID          = args[0];
        let channelName     = args[1];
        let chaincodeName   = args[2];
        args[3] = args[3].replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t");
        let params          = JSON.parse(args[3])["Args"];

        const walletPath = path.join(__dirname, 'wallet');
        const wallet = new FileSystemWallet(walletPath);

        const userExists = await wallet.exists(userID);
        if (!userExists) {
            console.log(`An identity for the user ${userID} does not exist in the wallet`);
            return;
        }

        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: userID, discovery: { enabled: true, asLocalhost: true } });

        const network = await gateway.getNetwork(channelName);
        const contract = network.getContract(chaincodeName);

        const result = await contract.evaluateTransaction(...params);
        console.log(`Transaction has been evaluated, result is: ${result.toString()}`);;

        callback(null, result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        callback(error, null);
    }
}

module.exports.invoke = async (args, callback) => {
    try {
        let userID          = args[0];
        let channelName     = args[1];
        let chaincodeName   = args[2];

        args[3] = args[3].replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t");
        let params          = JSON.parse(args[3])["Args"];
        console.log("userId:" + userID);
        const walletPath = path.join(__dirname, 'wallet');
        const wallet = new FileSystemWallet(walletPath);

        const userExists = await wallet.exists(userID);
        if (!userExists) {
            console.log(`An identity for the user ${userID} does not exist in the wallet`);
            return;
        }
        const gateway = new Gateway();
        await gateway.connect(ccp,{ wallet, identity: userID, discovery: { enabled: true, asLocalhost: true } });
        const network = await gateway.getNetwork(channelName);
        const contract = network.getContract(chaincodeName);
        const result = await contract.submitTransaction(...params);
        console.log(`Transaction has been invoked, result is: ${result.toString()}`);
        await gateway.disconnect();

        callback(null, result);
    } catch (error) {
        console.error(`Failed to invoke transaction: ${error}`);
        callback(error, null);
    }
}