# nvm use 12.13.0
# cd front && forever start node_modules/@angular/cli/bin/ng serve

fdir="$( cd "$(dirname "$0")" ; pwd -P )"
fname=sp_front

function front_init
{
	#source $fdir/../common/logger.sh
	source $NVM_DIR/nvm.sh
	set -a
	. $fdir/.env
	set +a
	nvm install 12.13.0
}

function front_build
{
	info "Build for front-end"
	cd $fdir
	nvm install 12.13.0
	npm install
    echo "{\"API_SERVER_ADDRESS\":\"http://${API_SERVER_ADDRESS}\", \"MOBIUS_SERVER_ADDRESS\":\"http://${MOBIUS_SERVER_ADDRESS}\"}"
	echo "{\"API_SERVER_ADDRESS\":\"http://${API_SERVER_ADDRESS}\", \"MOBIUS_SERVER_ADDRESS\":\"http://${MOBIUS_SERVER_ADDRESS}\"}" > $fdir/src/assets/env.json
}

function front_stop
{
    info "Front-end stop"
	pm2 stop $fname
}

function front_start
{
	info "Front-end start"	
	echo ${FRONT_SERVER_IP}:${FRONT_SERVER_PORT}
	echo "cd $fdir && pm2 start \"node_modules/@angular/cli/bin/ng serve --port ${FRONT_SERVER_PORT} --disable-host-check --host ${FRONT_SERVER_IP}\" --name $fname"
	cd $fdir && pm2 start "node_modules/@angular/cli/bin/ng serve --port ${FRONT_SERVER_PORT} --disable-host-check --host ${FRONT_SERVER_IP}" --name $fname
}

function front_clean
{
	info "Clean up front-end"
    pm2 delete $fname
	rm -rf $fdir/node_modules
}

function front_all
{
    front_clean
    front_build
	front_start
}

function main
{
	front_init
	case $1 in
		all | build | rebuild | start | stop | clean )
			cmd=$1
			shift
			front_$cmd $@
		;;
		*)
			error "$1 : Not supported command"
			info "all | build | rebuild | start | stop | clean"
			exit 1
		;;
	esac
}

main $@
