import { Component, OnInit, Input, Inject } from '@angular/core';
import { MobiusGetDht } from '../../models/mobius.model';
import { DeviceService } from 'src/app/services/device.service';
import { LocalStorageService } from 'src/app/services/localstorage.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-imagecard',
  templateUrl: './imagecard.component.html',
  styleUrls: ['./imagecard.component.css']
})
export class ImagecardComponent implements OnInit {
  @Input() title: string;
  @Input() deviceId: string;
  @Input() image: string;
  @Input() position: string;
  @Input() desc: string;

  @Input('dht') footerTitle: MobiusGetDht;
  @Input() cardIndex: number;

  constructor(private deviceService: DeviceService, private storage: LocalStorageService, private dialog:MatDialog) {
  }

  remove() {
    this.deviceService.deleteDeviceContent(this.deviceId);
  }

  openCertReRegisterDialog() {
    const dialogRef = this.dialog.open(DeviceCardDialog, {
      width: '800px',
      height: '700px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
      }
    });
  }

  ngOnInit() {}
}

@Component({
  selector: 'device-card-dialog',
  templateUrl: './device-card-dialog.html'
})
export class DeviceCardDialog {
  necessaryForm = new FormControl("", [Validators.required]);
  private fileInput: string;

  constructor (
    public dialogRef: MatDialogRef<DeviceCardDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    // private service: ServerPlatformService
  ) {
  }

  getFormError() {
    return this.necessaryForm.hasError('required') ? '필수로 입력해야합니다.' : '';
  }

  onFileChange(event) {
    var file = event.target.files[0];
    var reader: FileReader = new FileReader();
    reader.onload = (e) => {
      this.fileInput = reader.result.toString();
    };
    reader.readAsText(file);
  }

  onButtonClick(code: string) {
    // this.service.pushServerList({uid: uid, cert: this.fileInput} as ServerPlatform)
  }
}
