import { Component, OnInit, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { ServerPlatformService } from '../../services/serverplatform.service';
import { LocalStorageService } from '../../services/localstorage.service';
import { BlockchainPlatformService } from 'src/app/services/blockchain/blockchain-platform.service';
import { Router } from '@angular/router';
declare const swal: any;

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  public color: string;
  public menuItems: object;
  public activeFontColor: string;
  public normalFontColor: string;
  public dividerBgColor: string;
  
  constructor(public dialog: MatDialog, private serverPlatform :ServerPlatformService, private localStorage: LocalStorageService, private pService: BlockchainPlatformService, private router: Router) {
    this.menuItems = [
      {
        id: "mserver",
        title: "FEMS management",
        icon: "dashboard",
      }/*,
      {
        path: "profile",
        title: "User Profile",
        icon: "person",
        children: null
      },
      {
        path: "notification",
        title: "Notification",
        icon: "notifications",
        children: null
      },*/
    ];

    this.serverPlatform.getInitServerList().then((list) => {
      if (list != null) {
        var children = list.map((t,i) => {return {title: t, icon: 'S'+i.toString()}});
        this.menuItems[0].children = children;
      }
    });

    this.serverPlatform.ServerLists$.subscribe(data => {
      var children = data.map((t,i) => {return {title: t, icon: 'S'+i.toString()}});
      this.menuItems[0].children = children;
    });
  }

  ngOnInit() {
    this.color = '#fff';
    this.activeFontColor = 'rgba(0,0,0,.6)';
    this.normalFontColor = 'rgba(0,0,0,.6)';
    this.dividerBgColor = 'rgba(0,0,0,.1)';
  }

  onLogoClick() {
    this.router.navigate(['/main']);
  }

  onServerPlatformClick(platformId: string) {
    this.serverPlatform.selectServerItem(platformId);
  }

  removeServerPlatformItem(title: string) {
    this.serverPlatform.deleteServerListItem(title);
  }

  openServerPlatformDialog() {
    const dialogRef = this.dialog.open(ServerPlatformDialog, {
      width: '500px',
      height: '450px'
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log(result);
    })

  }

  reRegisterCertificate(platformId: string) {
    if (this.pService.ReRegisterRequest(this.localStorage.Get("user_token"),this.localStorage.Get("user_id"),platformId)) {
      // NOTE: Send verification code
      swal({
        title:"인증서 재등록", 
        text:"이메일로 발급받은 인증코드를 입력해주세요.", 
        input: "text",
        confirmButtonText: "생성",
        showCancelButton: true,
      }).then((result) => {
        // NOTE: Verify
        var cert = this.pService.VerifyInReRegister(result, this.localStorage.Get("user_token"),platformId);
        console.log(cert);
      });
    }
    swal("Error!","email send error", "warning");
  }
}

@Component({
  selector: 'server-platform-dialog',
  templateUrl: './server-platform-dialog.html'
})
export class ServerPlatformDialog {
  necessaryForm = new FormControl("", [Validators.required]);
  private fileInput: string;

  constructor (
    public dialogRef: MatDialogRef<ServerPlatformDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: ServerPlatformService
  ) {
  }

  getFormError() {
    return this.necessaryForm.hasError('required') ? '필수로 입력해야합니다.' : '';
  }

  onFileChange(event) {
    var file = event.target.files[0];
    var reader: FileReader = new FileReader();
    reader.onload = (e) => {
      this.fileInput = reader.result.toString();
    };

    reader.readAsText(file);
  }

  onButtonClick(uid: string) {
    this.service.pushServerList(uid, this.fileInput);
  }
}
