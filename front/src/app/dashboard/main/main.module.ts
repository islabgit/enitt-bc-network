import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';

import {MainComponent} from './main.component';

import { from } from 'rxjs';

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
  ],
  entryComponents: [MainComponent]
})
export class MainModule { }
