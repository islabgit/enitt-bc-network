import { Component, OnInit, Inject } from "@angular/core";
import { MobiusService } from "../../services/mobius.service";
import { MobiusGetDht } from "../../models/mobius.model";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { LocalStorageService } from "src/app/services/localstorage.service";
import { DeviceService, DeviceContent } from "src/app/services/device.service";
import { ServerPlatformService } from '../../services/serverplatform.service';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  public selectedServerPlatform$: string;
  public deviceContents$: DeviceContent[];
  public MobiusDht$: MobiusGetDht[];
  
  constructor(public mobius: MobiusService, public dialog: MatDialog, private deviceContService: DeviceService, private serverPlatform: ServerPlatformService) {
    this.deviceContService.DeviceList$.subscribe(data => this.deviceContents$ = data);
    const cids: string[] = this.deviceContents$.map(content => content.deviceId);
    this.deviceContService.DeviceDHT$.subscribe(res => this.MobiusDht$ = res);
    this.serverPlatform.PlatformName$.subscribe((serverName) => {this.selectedServerPlatform$ = serverName});
  }

  ngOnInit() {}

  openAddDiviceDialog() {
    const dialogRef = this.dialog.open(AddDeviceDialog, {
      width: '800px',
      height: '700px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
      }
    });
  }
}

// Dialog Component
@Component({
  selector: 'home-device-dialog',
  templateUrl: './home-device-dialog.html',
})
export class AddDeviceDialog {
  necessaryForm1 = new FormControl("", [Validators.required]);
  necessaryForm2 = new FormControl("", [Validators.required]);
  necessaryForm3 = new FormControl("", [Validators.required]);
  
  private fileInput: string;

  constructor(
    public dialogRef: MatDialogRef<AddDeviceDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private deviceService: DeviceService,
    private deviceStorage: LocalStorageService
  ) {}

  getFormError() {
    if (this.necessaryForm1.hasError('required') || this.necessaryForm2.hasError('required') || this.necessaryForm3.hasError('required')) {
      return '필수로 입력해야합니다.';
    } else {
      return '';  
    }    
  }

  onFileChange(event) {
    var file = event.target.files[0];
    var reader: FileReader = new FileReader();
    reader.onload = (e) => {
      this.fileInput = reader.result.toString();
    };

    reader.readAsText(file);
  }
  
  applyDevice(getDeviceName: string, getDeviceId: string, getCameraAddr: string, getDeviceLocation?: string, getDesc?:string) {
    let contentTemp = {title: getDeviceName, deviceId: getDeviceId, cameraAddr: getCameraAddr, pubKey: this.fileInput,location: getDeviceLocation, desc: getDesc} as DeviceContent
    this.deviceService.pushDeviceContent(contentTemp);
    this.dialogRef.close();
  }
}

