export class MobiusGetDht {
  con: { temp: number, humidity: number;};
  cr: string;
  cs?: number;
  ct?: Date;
  et?: Date;
  lt?: Date;
  pi?: string;
  ri?: string;
  rn?: string;
  st?: number;
  ty?: number;
}

    // let testDeviceNode: DeviceContent[] = [
    //   {
    //     cameraAddr: 'http://172.30.1.21:8000/camera',
    //     mobiusId: 'trusthingz1',
    //     title: 'CCTV1',
    //     desc: '부산 에코델타시티 델타루 지능형 CCTV Serial No.1',
    //     location: '델타루, 부산에코델타시티'
    //   },
    //   // {
    //   //   cameraAddr: 'http://5pecia1.iptime.org:48000/camera',
    //   //   mobiusId: 'trusthingz2',
    //   //   title: 'CCTV2',
    //   //   desc: '부산 에코델타시티 델타루 지능형 CCTV Serial No.2',
    //   //   location: '델타루, 부산에코델타시티'
    //   // },
    //   {
    //     cameraAddr: 'http://172.30.1.145:8000/camera/',
    //     mobiusId: 'trusthingz3',
    //     title: 'CCTV3',
    //     desc: '부산 에코델타시티 델타루 지능형 CCTV Serial No.3',
    //     location: '델타루, 부산에코델타시티'
    //   }
    // ];