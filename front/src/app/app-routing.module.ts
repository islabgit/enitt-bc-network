import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MainRoutingModule } from './dashboard/main/main-routing.module';
import { ConfirmComponent } from './confirm/confirm.component';
import { ReissueComponent } from './reissue/reissue.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'confirm', component: ConfirmComponent },
  { path: 'reissue', component: ReissueComponent },
  { path: '**', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MainRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
