import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { BlockchainUserService } from '../services/blockchain/blockchain-user.service';
import { LocalStorageService } from '../services/localstorage.service';
declare const swal: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  private fileInput: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private uService: BlockchainUserService, private localStore: LocalStorageService) {
    this.localStore.Put("user_state", "register");
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(15)]],
      confirmPassword: ['', Validators.required]
    }, {
      validators: this.MustMatch('password', 'confirmPassword')
    });
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (FormGroup: FormGroup) => {
      const control = FormGroup.controls[controlName];
      const matchingControl = FormGroup.controls[matchingControlName];

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  onFileChange(event) {
    var file = event.target.files[0];
    var reader: FileReader = new FileReader();
    reader.onload = (e) => {
      this.fileInput = reader.result.toString();
    };

    reader.readAsText(file);
  }

  submit(email: string, passwd: string) {
    this.uService.IsRegistered(email, passwd, this.fileInput).then(
      (res) => {
        if(!res) {  //NOTE: True means some one has the id. so we need use false for new user
          swal("전송완료!","입력한 이메일에서 인증코드를 확인하세요", "success");
          this.uService.EnrollRequest(email).then(
            (result) => {
              if (result) {
                this.router.navigate(['/confirm']);      
              } else {
                swal("에러!","인증코드 발급요청 실패!", "warning");
                this.router.navigate(['/login']);
              }
            }
          );
        } else {
          swal("에러!","이미 가입된 이메일입니다", "warning");
          this.router.navigate(['/login']);
        }
      }, (err) => {
        swal("에러!","이메일을 확인하세요", "warning");
        this.router.navigate(['/login']);
      }
    );
  }
}
