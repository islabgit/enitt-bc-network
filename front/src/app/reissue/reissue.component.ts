import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { BlockchainUserService } from '../services/blockchain/blockchain-user.service';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/localstorage.service';
declare const swal: any;

@Component({
  selector: 'app-reissue',
  templateUrl: './reissue.component.html',
  styleUrls: ['./reissue.component.css']
})
export class ReissueComponent implements OnInit {
  registerForm: FormGroup;
  private fileInput: string;

  constructor(private formBuilder: FormBuilder, private uService: BlockchainUserService, private router: Router, private localStore: LocalStorageService) {
    this.localStore.Put("user_state", "re_enroll");
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(15)]],
      confirmPassword: ['', Validators.required]
    }, {
      validators: this.MustMatch('password', 'confirmPassword')
    });
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (FormGroup: FormGroup) => {
      const control = FormGroup.controls[controlName];
      const matchingControl = FormGroup.controls[matchingControlName];

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  onFileChange(event) {
    var file = event.target.files[0];
    var reader: FileReader = new FileReader();
    reader.onload = (e) => {
      this.fileInput = reader.result.toString();
    };
    
    reader.readAsText(file);
  }

  onSubmit(email: string, passwd: string) {
    this.uService.IsRegistered(email, passwd, this.fileInput).then((res) => {
      if(res) {
          this.uService.EnrollRequest(email).then(
            (result) => {
              if (result) {
                this.router.navigate(['/confirm']);      
              } else {
                swal("Error!","enroll request error!", "warning");
                this.router.navigate(['/login']);
              }
            }
          );
      } else {
        swal("Error!","not enrolled email!", "warning");
        this.router.navigate(['/login']);
      }
    });
  }
}
