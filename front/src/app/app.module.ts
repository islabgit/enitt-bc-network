import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';

import { SidebarComponent, ServerPlatformDialog } from './dashboard/sidebar/sidebar.component';
import { MainComponent } from './dashboard/main/main.component';
import { HomeComponent, AddDeviceDialog } from './dashboard/home/home.component';
import { ImagecardComponent, DeviceCardDialog } from './util/device-card/imagecard.component';
import { NavbarComponent } from './util/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { RegisterComponent } from './register/register.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { ReissueComponent } from './reissue/reissue.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    ServerPlatformDialog,
    DeviceCardDialog,
    MainComponent,
    HomeComponent,
    AddDeviceDialog,
    ImagecardComponent,
    NavbarComponent,
    RegisterComponent,
    ConfirmComponent,
    ReissueComponent,
  ],
  entryComponents: [
    AddDeviceDialog,
    ServerPlatformDialog,
    DeviceCardDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    StorageServiceModule,

    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatListModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
