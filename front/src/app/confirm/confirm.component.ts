import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MongodbService } from '../services/mongodb.service';
import { BlockchainUserService } from '../services/blockchain/blockchain-user.service';
import { LocalStorageService} from '../services/localstorage.service';
declare const swal: any;

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  private state:string;

  constructor(private router: Router, private mongodb: MongodbService, private uService : BlockchainUserService, private localStore : LocalStorageService) {
    this.state = this.localStore.Get("user_state");
    // "re_enroll"
  }

  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  submit(code: string) {
    if(this.state == "register") {
      this.uService.EmailVerifyInEnroll(code).then(
        (data) => {
          if(data) {
            this.uService.Enroll().then((res) => {
              res ? swal("Enrolled!","successfully enrolled", "success") : swal("Error!","enrolled error", "warning");
            })
          } else {
            swal("Error!","verification code verify error", "warning");
          }
        }
      );
    } else if(this.state == "re_enroll") {
      this.uService.Reissue(code).then((res) => {
        res ? swal("Re-Enrolled!","successfully enrolled", "success") : swal("Error!","enrolled error", "warning");
      })
    }
    this.router.navigate(['/login']);
  }

  ngOnInit() {}

}
