import { TestBed } from '@angular/core/testing';

import { MobiusService } from './mobius.service';

describe('MobiusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MobiusService = TestBed.get(MobiusService);
    expect(service).toBeTruthy();
  });
});
