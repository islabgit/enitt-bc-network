import { Injectable } from '@angular/core';
import * as emailjs from 'emailjs-com';
import { MongodbService } from './mongodb.service';
import { EmailJSResponseStatus } from 'emailjs-com/source/models/EmailJSResponseStatus';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  templateParams = {
    to_user_email: 'security_plane@smartm2m.co.kr',  //Test mail
    platform_name: 'Miletus platform',
    confirm_code: 'test'
  }

  constructor(private mongodb: MongodbService) {}

  async SendMail(mailAddr: string, token: string): Promise<EmailJSResponseStatus> {
    this.templateParams.to_user_email = mailAddr;
    this.templateParams.confirm_code = token;

    let result;

    emailjs.init('user_OXFAzpl85cVZquhVtBgCa');
    return emailjs.send('default_service','template_aJmxShlJ',this.templateParams);
  }

  // makeConfirmCode(lengthOfCode: number) {
  //   let possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890*&^%$#@!~";
  //   let text = "";
  //   for (let i = 0; i < lengthOfCode; i++) {
  //     text += possible.charAt(Math.floor(Math.random() * possible.length));
  //   }
  //     return text;
  // }
}
