import { TestBed } from '@angular/core/testing';

import { ServerPlatformService } from './serverplatform.service';

describe('PlatformserverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServerPlatformService = TestBed.get(ServerPlatformService);
    expect(service).toBeTruthy();
  });
});
