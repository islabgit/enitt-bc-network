import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { LocalStorageService } from './localstorage.service';
import { BlockchainDeviceService } from './blockchain/blockchain-device.service';
import { MobiusService } from './mobius.service';
import { MobiusGetDht } from '../models/mobius.model';

export class DeviceContent {
  cameraAddr: string;
  deviceId: string;
  title: string;
  pubKey?: string;
  desc?: string;
  location?: string;
}

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  private deviceList = new BehaviorSubject<DeviceContent[]>([]);
  private getDeviceTemp: DeviceContent[] = [];
  public DeviceList$ = this.deviceList.asObservable();

  private deviceDHT = new BehaviorSubject<MobiusGetDht[]>([]);
  public DeviceDHT$ = this.deviceDHT.asObservable();

  constructor(private localStore: LocalStorageService, private dService: BlockchainDeviceService, private mobiusService: MobiusService){}

  initDeviceContents(deviceContetnts: DeviceContent[]) {
    this.getDeviceTemp = deviceContetnts;
    this.deviceList.next(this.getDeviceTemp);
    this.mobiusService.getMobiusDht(deviceContetnts.map(res => res.deviceId)).subscribe(res => {this.deviceDHT.next(res)});
  }

  pushDeviceContent(content: DeviceContent) {
    this.dService.Enroll(content.deviceId,content.title,content.pubKey,content.location,content.desc,content.cameraAddr).then(
      (res) => {
      if(res) {
        this.getDeviceTemp.push(content);
        this.deviceList.next([...this.getDeviceTemp]);
      }
    }, (err) => {
      console.log(err);
    });
  }

  deleteDeviceContent(title: string) {
    this.dService.Remove(title).then(
      (res) => {
        var deleteDeviceIndex: number;
        this.getDeviceTemp.forEach((devices, index) => {
          if (res.indexOf(devices.deviceId) == -1) {
            deleteDeviceIndex = index;
          }
        });
        this.getDeviceTemp.splice(deleteDeviceIndex);
        this.deviceList.next([...this.getDeviceTemp]);
      });
  }

}
