import { TestBed } from '@angular/core/testing';

import { BlockchainUserService } from './blockchain-user.service';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlockchainUserService = TestBed.get(BlockchainUserService);
    expect(service).toBeTruthy();
  });
});
