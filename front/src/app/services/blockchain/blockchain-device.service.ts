import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { BlockchainServerIp } from '../../../environments/environment';
import { LocalStorageService } from '../localstorage.service';
import { DeviceContent } from '../device.service';
import * as saver from 'file-saver';
import * as env from '../../../assets/env.json';

interface HttpJSONResponse {
  state?: string;
  token?: string;
  is_valid?: boolean;
  certification?: string;
  iot_devices?: string;
}

@Injectable({
  providedIn: 'root'
})
export class BlockchainDeviceService {

  constructor(private http:HttpClient, private localStore: LocalStorageService) { }

  async Enroll(deviceId: string, nickname:string, pubKey:string, localtion:string, description:string, resourceAddress:string): Promise<boolean> {
    var token =this.localStore.Get("user_token");
    var userId = this.localStore.Get("user_id");
    var platformId = this.localStore.Get("platform_id");

    const resource = "iot_device/enroll";
    const body = {"token":token, "user_id":userId, "platform_id": platformId,"device_id": deviceId, "nickname":nickname, "public_key": pubKey, "location":localtion, "description":description, "resourceAddress":resourceAddress};

    let result;
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        if(data.certification != undefined) {
          var blob = new Blob(['-----BEGIN CERTIFICATE-----\n'+data.certification + '\n-----END CERTIFICATE-----'], {type: "text/plain;charset=utf-8"});
          saver.saveAs(blob, "ssm"+new Date().getTime().toString() + ".crt");
          result = true;
        } else {
          result = false;
        }
      }, (err) => {
        console.log(err);
        result = false;
      }
    );
    return result;
  }

  async ReRegisterRequest(token: string, userId: string, platformId: string, deviceId: string): Promise<boolean> {
    const resource = "iot_device/verificode_code/deploy";
    const body = {"token":token, "user_id":userId, "platform_id": platformId,"device_id": deviceId};

    let result;
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.state;
      }
    );
    return result == "success"? true: false;
  }

  async VerifyInReRegister(code: string, userId: string, platformId: string, deviceId: string): Promise<string> {
    const resource = "iot_device/verification_code/is_valid";
    const body = {"verification_code":code, "user_id":userId, "platform_id": platformId,"device_id": deviceId};

    let result;
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data;
      }
    );
    console.log(result);
    return result;
  }

  async Remove(deviceId: string): Promise<string[]> {
    var token =this.localStore.Get("user_token");
    var userId = this.localStore.Get("user_id");
    var platformId = this.localStore.Get("platform_id");
    
    const resource = "iot_device/remove";
    const body = {"token":token, "user_id":userId, "platform_id": platformId,"device_id": deviceId};

    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.iot_devices;
      }
    );
    return result;
  }
}
