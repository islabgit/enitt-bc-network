import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { BlockchainServerIp } from '../../../environments/environment';
import { MailService } from '../mail.service';
import { LocalStorageService } from '../localstorage.service';
import * as saver from 'file-saver';
import * as rs from 'jsrsasign';
import * as env from '../../../assets/env.json';

class User {
  id: string;
  password: string;
}

interface HttpJSONResponse {
  state?: string;
  token?: string;
  is_valid?: boolean;
  is_exist?: boolean;
  certification?: string;
  cacertification?: string;
  iot_platforms?: string;
}

@Injectable({
  providedIn: 'root'
})

export class BlockchainUserService {

  constructor(private http: HttpClient, private mail: MailService, private localStore: LocalStorageService) {
  }

  async IsRegistered(id: string, passwd: string, pubKey: string): Promise<boolean> {
    const resource = "user/is_exist";
    const body = {"user_id": id};

    this.localStore.Put("user_id", passwd);
    this.localStore.Put("user_password", passwd);
    this.localStore.Put("user_pub_key", pubKey);

    let result;
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.is_exist;
      }, (err) => {
        console.log(err);
        result = null;
      }
    );
    return result;
  }

  async EnrollRequest(id: string): Promise<boolean> {
    const resource = "user/verification_code/deploy";
    const body = {"user_id": id} ;
    let result;

    this.localStore.Put("user_email", id);
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.state;
      }, (err) => {
        console.log(err);
        result = "fail"
      }
    );
    return result == "success" ? true: false;
  }

  async EmailVerifyInEnroll(code: string): Promise<boolean> {
    var email = this.localStore.Get("user_email");
    const resource = "user/verification_code/is_valid";
    const body = {"verification_code": code, "user_id": email};
    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.is_valid;
      }
    );
    return result;
  }

  async Enroll(): Promise<boolean> {
    var email = this.localStore.Get("user_email");
    var password = this.localStore.Get("user_password");
    var pubKey = this.localStore.Get("user_pub_key");

    const resource = "user/enroll";
    const body = {"user_id": email, "password": password, "public_key": pubKey};

    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        var blob = new Blob(['-----BEGIN CERTIFICATE-----\n'+data.certification + '\n-----END CERTIFICATE-----'], {type: "text/plain;charset=utf-8"});
        var blob2 = new Blob(['-----BEGIN CERTIFICATE-----\n'+data.cacertification + '\n-----END CERTIFICATE-----'], {type: "text/plain;charset=utf-8"});
        //saver.saveAs(blob, "mobiusU"+new Date().getTime().toString() + ".crt");
        saver.saveAs(blob2, "rootCA"+new Date().getTime().toString() + ".crt");
        result = true;
      }, (err) => {
        console.log(err);
        result = false;
      }
    );
    return result;
  }


  async Login(userId: string, userPw: string, privKey: string): Promise<string> {
    const resource = "user/token/deploy";
    var nowTime = new Date().getTime().toString();
    
    var key = rs.KEYUTIL.getKey(privKey).prvKeyHex;
    var sig = new rs.KJUR.crypto.Signature({alg: "SHA256withECDSA"});
    sig.init({d: key, curve: "secp256r1"});
    sig.updateString(userId + userPw + nowTime);
    var signature = rs.hex2b64(sig.sign());

    const body = {"user_id": userId, "password": userPw, "timestamp": nowTime, "signature": signature};
    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        if(data.token == undefined) {
          result = "fail";
        } else {
          this.localStore.Put("user_token", data.token);
          result = data.token;
        }
      }, (err) => {
        console.log(err);
        result = "fail";
      }
    );
    return result;
  }

  base64toHEX(base64: string): string {
    var raw = atob(base64);
    var hex = '';
    for (var i = 0; i < raw.length; i++ ) {
      var _hex = raw.charCodeAt(i).toString(16)
      hex += (_hex.length==2?_hex:'0'+_hex);
    }
    return hex
  }

  async IsValidToken(code: string, email: string): Promise<boolean> {
    const resource = "user/token/is_valid";
    const body = {"token": code, "user_id": email};

    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.is_valid;
      }
    );
    console.log(result);
    return result;
  }

  async GetInfo(): Promise<string[]> {
    const resource = "user/info";

    var token = this.localStore.Get("user_token");
    var userId = this.localStore.Get("user_id");
    const body = {"token": token, "user_id": userId};

    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.iot_platforms;
      }, (err) => {
        result = null;
      }
    );
    return result;
  }

  async Reissue(code: string): Promise<boolean> {
    var userId = this.localStore.Get("user_email");
    var userPw = this.localStore.Get("user_password");
    var pubKey = this.localStore.Get("user_pub_key");

    const resource = "user/re_enroll";

    const body = {"verification_code": code, "user_id": userId, "password": userPw, "public_key": pubKey};
    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        //var blob = new Blob([data.certification], {type: "text/plain;charset=utf-8"});
        //saver.saveAs(blob, "mobiusU"+new Date().getTime().toString() + ".crt");

        var blob2 = new Blob(['-----BEGIN CERTIFICATE-----\n'+data.cacertification + '\n-----END CERTIFICATE-----'], {type: "text/plain;charset=utf-8"});        
        saver.saveAs(blob2, "rootCA"+new Date().getTime().toString() + ".crt");

        result = true;
      }, (err) => {
        console.log(err);
        result = false;
      }
    );
    return result;
  }
}
