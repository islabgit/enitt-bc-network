import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { BlockchainServerIp } from '../../../environments/environment';
import { LocalStorageService } from '../localstorage.service';
import { DeviceContent } from '../device.service';
import * as saver from 'file-saver';
import * as env from '../../../assets/env.json';

class Platform {
  id: string;
  password: string;
}

interface HttpJSONResponse {
  state?: string;
  token?: string;
  is_valid?: boolean;
  certification?: string;
  iot_platforms?: string;
  iot_devices?: any;
}

@Injectable({
  providedIn: 'root'
})
export class BlockchainPlatformService {

  constructor(private http:HttpClient, private localStore: LocalStorageService) {}

  async Enroll(pubkey: string, platformId: string): Promise<string> {
    var code = this.localStore.Get("user_token")
    var userId = this.localStore.Get("user_id");

    const resource = "iot_platform/enroll";
    const body = {"token": code, "public_key": pubkey, "user_id": userId, "platform_id": platformId};

    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        if(data.certification != undefined) {
          var blob = new Blob(['-----BEGIN CERTIFICATE-----\n'+data.certification + '\n-----END CERTIFICATE-----'], {type: "text/plain;charset=utf-8"});
          saver.saveAs(blob, "fems"+new Date().getTime().toString() + ".crt");
        }
        result = platformId;
      }
    );
    return result;
  }

  async ReRegisterRequest(code: string, userId: string, platformId: string): Promise<boolean> {
    const resource = "iot_platform/verificode_code/deploy";
    const body = {"token": code, "user_id": userId, "platform_id": platformId};

    let result;

    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        result = data.state;
      }
    );
    console.log(result);
    return result == "success" ? true : false;
  }

  async VerifyInReRegister(code: string, userId: string, platformId: string): Promise<string> {
    const resource = "iot_platform/verification_code/is_valid";
    const body = {"verification_code": code, "user_id": userId, "platform_id": platformId};

    let result;
    // let resToken;
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        // result = data.is_valid;
        result = data;
      });

    console.log(result);
    return result;
  }

  async Remove(platformId: string): Promise<string[]> {
    var code = this.localStore.Get("user_token");
    var userId = this.localStore.Get("user_id");

    const resource = "iot_platform/remove";
    const body = {"token": code, "user_id": userId, "platform_id": platformId};

    let result;
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        if(data.iot_platforms != undefined) {
          result = data.iot_platforms;
        } else {
          console.log(data);
        }
      }, (err) => {
        console.log(err);
      });
    return result;
  }

  async GetInfo(platformId: string): Promise<DeviceContent[]> {
    var code = this.localStore.Get("user_token");
    var userId = this.localStore.Get("user_id");

    const resource = "iot_platform/info/devices";
    const body = {"token": code, "user_id": userId, "platform_id": platformId};

    var deviceResult :DeviceContent[] = [];
    await this.http.post<HttpJSONResponse>(env.API_SERVER_ADDRESS + '/api/v1/' + resource, body).toPromise().then(
      (data) => {
        var result = data.iot_devices;
        result.forEach((device) => {
          device.Record.device_id = device.Record.uid.split('_'+device.Record.parent+'_')[1];
          deviceResult.push({
            title: device.Record.nickname,
            deviceId: device.Record.device_id,
            cameraAddr: device.Record.resource_address,
            location: device.Record.location,
            desc: device.Record.description
          })
        });
      }
    );
    return deviceResult;
  }
}
