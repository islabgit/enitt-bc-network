import { TestBed } from '@angular/core/testing';

import { BlockchainPlatformService } from './blockchain-platform.service';

describe('PlatformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlockchainPlatformService = TestBed.get(BlockchainPlatformService);
    expect(service).toBeTruthy();
  });
});
