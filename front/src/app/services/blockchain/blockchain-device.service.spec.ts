import { TestBed } from '@angular/core/testing';

import { BlockchainDeviceService } from './blockchain-device.service';

describe('DeviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlockchainDeviceService = TestBed.get(BlockchainDeviceService);
    expect(service).toBeTruthy();
  });
});
