import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CodeNode } from 'source-list-map';

@Injectable({
  providedIn: 'root'
})
export class MongodbService {
  public UserMail: string;

  constructor(private http: HttpClient) {}

  public UserRegister(email: string, password: string){
    this.UserMail = email;
    var reqBody = {
      "register" : {
        "email" : email,
        "password" : password
      }
    };
    this.httpPost(reqBody)
  }

  public UserConfirmReq(email: string, code: string){
    this.UserMail = email;
    var reqBody = {
      "confirm_req" : {
        "email" : email,
        "confirm_code": code
      }
    }
    this.httpPost(reqBody)
  };

  public UserConfirm(email: string, checkCode: string){
    this.UserMail = email;
    var reqBody = {
      "confirm" : {
        "email" : email,
        "confirm_code": checkCode
      }
    }
    this.httpPost(reqBody)
  };

  httpPost(reqBody: any){
    this.http.post('http://localhost:3100/api/v1/db/trusthingzdb', reqBody).subscribe(data => {
      console.log(data);
    })
  };
}
