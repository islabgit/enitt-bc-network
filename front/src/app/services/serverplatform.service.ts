import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LocalStorageService } from './localstorage.service';
import { BlockchainUserService } from './blockchain/blockchain-user.service';
import { BlockchainPlatformService } from './blockchain/blockchain-platform.service';
import { DeviceService, DeviceContent } from './device.service';
import { Router } from '@angular/router';
declare const swal: any;


@Injectable({
  providedIn: 'root'
})
export class ServerPlatformService {
  private serverLists = new Subject<string[]>();
  private getServerListTemp : string[] = [];
  ServerLists$ = this.serverLists.asObservable();

  private platformName = new Subject<string>();
  public PlatformName$ = this.platformName.asObservable();

  constructor(private localStorage: LocalStorageService, private deviceService: DeviceService, private uService: BlockchainUserService, private pService: BlockchainPlatformService, private router: Router, private device: DeviceService) {}

  getInitServerList(): Promise<string[]> {
    return this.uService.GetInfo().then((platlist) => {
      if(platlist == null || platlist == undefined) {
        swal("Error!","iot platform data get error", "warning");
        this.localStorage.Del("user_token");
        this.localStorage.Del("user_id");

        this.router.navigate(['/login']);
        return null;
      } else {
        this.getServerListTemp = platlist;
        return this.getServerListTemp;
      }
    }, (err) => {
      this.localStorage.Del("user_token");
      this.localStorage.Del("user_id");

      this.router.navigate(['/login']);
      swal("Error!","please login again", "warning");
      return null;
    });
  }

  pushServerList(pid: string, pubKey: string) {
    this.pService.Enroll(pubKey, pid).then((res) => {
      this.getServerListTemp.push(res);
      this.serverLists.next([...this.getServerListTemp]);
    });
  }

  deleteServerListItem(title: string) {
    this.pService.Remove(title).then((platlist) => {
      console.log(platlist);
      if(platlist != null) {
        this.getServerListTemp = platlist;
        this.serverLists.next([...this.getServerListTemp]);
      }
    }, () => {
      swal("Error!","iot platform data get error", "warning");
      this.localStorage.Del("user_token");
      this.localStorage.Del("user_id");

      this.router.navigate(['/login']);
    });
  }

  selectServerItem(platformId: string) {
    this.localStorage.Put("platform_id", platformId);
    var index = this.getServerListTemp.indexOf(platformId);
    this.pService.GetInfo(platformId).then((data) => {
      this.platformName.next(platformId);  
      this.device.initDeviceContents(data);
    }, (err) => {

    });
  }
}
