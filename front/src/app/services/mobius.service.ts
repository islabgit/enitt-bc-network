import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, interval, forkJoin } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { MobiusGetDht } from "../models/mobius.model";
// import { MobiusServerIp } from 'src/environments/environment';
import * as env from '../../assets/env.json';

@Injectable({
  providedIn: "root"
})
export class MobiusService {
  constructor(private http: HttpClient) {}

  public getMobiusDht(addr: string[]): Observable<MobiusGetDht[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "X-M2M-RI": "smartcity_test",
        "X-M2M-Origin": "smartcity_test_origin"
      })
    };

    var observableBatch: Observable<MobiusGetDht>[] = [];
    addr.forEach((cid) => {
      console.log("test");
      observableBatch.push(this.http.get<object>(env.MOBIUS_SERVER_ADDRESS + '/Mobius/' + cid + '/cnt-dht/latest', httpOptions).pipe( map(tem => tem['m2m:cin'])))
    })

    var observ$: Observable<MobiusGetDht[]> = interval(1000).pipe(
      switchMap(() => forkJoin(
        observableBatch
      ))
    )
    return observ$;
  }
}
