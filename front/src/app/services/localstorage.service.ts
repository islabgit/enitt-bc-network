import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service'; //reference site: https://developer.mozilla.org/ko/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API
import { DeviceContent } from './device.service';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  public deviceContentsData$: DeviceContent[];
  deviceContetnsDataKey = 'deviceContents';

  constructor(@Inject(LOCAL_STORAGE) private storage:StorageService){
    this.deviceContentsData$ = this.storage.get(this.deviceContetnsDataKey) || [];
  }

  public getDeviceContent() : DeviceContent[] {
    this.deviceContentsData$ = this.storage.get(this.deviceContetnsDataKey) || [];
    return this.deviceContentsData$
  }

  public storeDeviceContent(contents : DeviceContent[]) {
    this.deviceContentsData$ = contents;
    this.storage.set(this.deviceContetnsDataKey, contents);
  }

  public pushDeviceContent(val: DeviceContent): DeviceContent[] {
    this.deviceContentsData$ = this.storage.get(this.deviceContetnsDataKey) || [];
    this.deviceContentsData$.push(val);

    this.storage.set(this.deviceContetnsDataKey, this.deviceContentsData$);
    return this.deviceContentsData$;
  }

  public removeDeviceContent(val: DeviceContent): any {
    if(this.storage.has(this.deviceContetnsDataKey)){
      console.log('The data is empty at Local storage');
    }

    this.deviceContentsData$ = this.storage.get(this.deviceContetnsDataKey) || [];

    var index = this.deviceContentsData$.indexOf(val);
    if (index > -1) {
      this.deviceContentsData$.splice(index, 1);
    }

    this.storage.set(this.deviceContetnsDataKey, this.deviceContentsData$);

    return this.deviceContentsData$;
  }

  public Put(key:string, value:any): void {
    return this.storage.set(key,value);
  }

  public Get(key:string): any {
    if (!this.storage.has(key)){
      return new Error("!property");
    }
    return this.storage.get(key);
  }

  public Del(key:string): any {
    if (!this.storage.has(key)){
      return new Error("!property");
    }
    return this.storage.remove(key);
  }
  
}
