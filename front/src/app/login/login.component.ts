import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { BlockchainUserService } from '../services/blockchain/blockchain-user.service';
import { LocalStorageService } from '../services/localstorage.service';
declare const swal: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  private fileInput: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private uService: BlockchainUserService, private localStore: LocalStorageService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(15)]],
    }, {});
  }

  onFileChange(event) {
    var file = event.target.files[0];
    var reader: FileReader = new FileReader();
    reader.onload = (e) => {
      this.fileInput = reader.result.toString();
    };
    
    reader.readAsText(file);
  }

  onReissue() {
    this.router.navigate(['/reissue']);
  }

  onSubmit(id: string, pw: string) {
    this.uService.Login(id, pw, this.fileInput).then(
      (data) => {
        if (data == "fail") {
          swal("Error!", "Login failed", "warning");
          this.router.navigate(['/login']);
        } else {
          this.localStore.Put("user_id", id);
          this.localStore.Put("user_token", data);
          this.router.navigate(['/main']);
        }
      }
    )
  }
}
