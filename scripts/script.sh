#!/bin/bash

ORDERER_CA="/opt/gopath/src/enitt-bc/crypto/ordererOrganizations/energy.org/orderers/orderer1.energy.org/msp/tlscacerts/tlsca.energy.org-cert.pem"
PEER1_ENITT_CA="/opt/gopath/src/enitt-bc/crypto/peerOrganizations/enitt.energy.org/peers/peer1.enitt.energy.org/tls/ca.crt"
PEER1_SC_CA="/opt/gopath/src/enitt-bc/crypto/peerOrganizations/sc.energy.org/peers/peer1.sc.energy.org/tls/ca.crt"
PEER1_SG_CA="/opt/gopath/src/enitt-bc/crypto/peerOrganizations/sg.energy.org/peers/peer1.sg.energy.org/tls/ca.crt"
PEER1_SW_CA="/opt/gopath/src/enitt-bc/crypto/peerOrganizations/sw.energy.org/peers/peer1.sw.energy.org/tls/ca.crt"

CHANNEL_NAME="$1"
DELAY="$2"
COUNTER=1
MAX_RETRY=10
LANGUAGE="golang"
CC_SRC_PATH="enitt-bc/chaincode/token"
CC_SRC_PATH2="enitt-bc/chaincode/sp"

declare -a ORGS=("enitt" "sc" "sg" "sw")

echo "Channel name : "$CHANNEL_NAME

# verify the result of the end-to-end test
verifyResult() {
    if [ $1 -ne 0 ]; then
        echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
        echo "========= ERROR !!! FAILED init network ==========="
        echo
        exit 1
    fi
}

setGlobals() {
    PEER=$1
    ORG=$2
    if [ $ORG = "enitt" ]; then
        CORE_PEER_LOCALMSPID="ENITTMSP"
        CORE_PEER_TLS_ROOTCERT_FILE=$PEER1_ENITT_CA
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/enitt-bc/crypto/peerOrganizations/enitt.energy.org/users/Admin@enitt.energy.org/msp
        if [ $PEER -eq 1 ]; then
            CORE_PEER_ADDRESS=peer1.enitt.energy.org:7051
        else
            CORE_PEER_ADDRESS=peer2.enitt.energy.org:8051
        fi
    elif [ $ORG = "sc" ]; then
        CORE_PEER_LOCALMSPID="SCMSP"
        CORE_PEER_TLS_ROOTCERT_FILE=$PEER1_SC_CA
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/enitt-bc/crypto/peerOrganizations/sc.energy.org/users/Admin@sc.energy.org/msp
        if [ $PEER -eq 1 ]; then
            CORE_PEER_ADDRESS=peer1.sc.energy.org:9051
        else
            CORE_PEER_ADDRESS=peer2.sc.energy.org:10051
        fi
    elif [ $ORG = "sg" ]; then
        CORE_PEER_LOCALMSPID="SGMSP"
        CORE_PEER_TLS_ROOTCERT_FILE=$PEER1_SG_CA
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/enitt-bc/crypto/peerOrganizations/sg.energy.org/users/Admin@sg.energy.org/msp
        if [ $PEER -eq 1 ]; then
            CORE_PEER_ADDRESS=peer1.sg.energy.org:11051
        else
            CORE_PEER_ADDRESS=peer2.sg.energy.org:12051
        fi        
    elif [ $ORG = "sw" ]; then
        CORE_PEER_LOCALMSPID="SWMSP"
        CORE_PEER_TLS_ROOTCERT_FILE=$PEER1_SW_CA
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/enitt-bc/crypto/peerOrganizations/sw.energy.org/users/Admin@sw.energy.org/msp
        if [ $PEER -eq 1 ]; then
            CORE_PEER_ADDRESS=peer1.sw.energy.org:13051
        else
            CORE_PEER_ADDRESS=peer2.sw.energy.org:14051
        fi        
    else
        echo "================== ERROR !!! ORG Unknown =================="
    fi
}

createChannel() {
    setGlobals 1 "enitt"

    peer channel create -o orderer1.energy.org:7050 -t 60s -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA --outputBlock ./channel-artifacts/$CHANNEL_NAME.block

    echo "===================== Channel '$CHANNEL_NAME' created ===================== "
    echo
}

joinChannel() {
    for org in "${ORGS[@]}"; do
        for peer in 1 2; do
            joinChannelWithRetry $peer $org
            echo "===================== peer${peer}.${org} joined channel '$CHANNEL_NAME' ===================== "
            sleep $DELAY
            echo
        done
    done
}

## Sometimes Join takes time hence RETRY at least 5 times
joinChannelWithRetry() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG

    set -x
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block >&log.txt
    res=$?
    set +x
    cat log.txt
    if [ $res -ne 0 -a $COUNTER -lt $MAX_RETRY ]; then
        COUNTER=$(expr $COUNTER + 1)
        echo "${PEER}.${ORG} failed to join the channel, Retry after $DELAY seconds"
        sleep $DELAY
        joinChannelWithRetry $PEER $ORG
    else
        COUNTER=1
    fi
    verifyResult $res "After $MAX_RETRY attempts, peer${PEER}.${ORG} has failed to join channel '$CHANNEL_NAME' "
}

updateAnchorPeers() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG

    set -x
    peer channel update -o orderer1.energy.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
    res=$?
    set +x

    cat log.txt
    verifyResult $res "Anchor peer update failed"
    echo "===================== Anchor peers updated for org '$CORE_PEER_LOCALMSPID' on channel '$CHANNEL_NAME' ===================== "
    sleep $DELAY
    echo
}

buildChaincode() {
    #mkdir -p $GOPATH/src/energy.org
    #cp -r ./chaincode/token "$_" && cd $GOPATH/src/energy.org/token && GOCACHE=on GO111MODULE=on go mod vendor
    #cp -r ./chaincode/token "$_" && cd $GOPATH/src/energy.org/token && go get
    cd $GOPATH/src/enitt-bc/chaincode/token && go get
}

installChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}
    set -x
    peer chaincode install -n tokencc -v ${VERSION} -l ${LANGUAGE} -p ${CC_SRC_PATH} >&log.txt
    peer chaincode install -n spcc -v ${VERSION} -l ${LANGUAGE} -p ${CC_SRC_PATH2} >&log2.txt
    # peer chaincode install -n tokencc -v 1.0 -l golang -p 'energy.org/token' >&log.txt
    res=$?
    set +x
    cat log.txt
    verifyResult $res "Chaincode installation on peer${PEER}.${ORG} has failed"
    echo "===================== Chaincode is installed on peer${PEER}.${ORG} ===================== "
    echo

    

    
}

instantiateChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}

    set -x
    # TODO: Add policy option 
    #peer chaincode instantiate -o orderer1.energy.org:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n tokencc -l ${LANGUAGE} -v ${VERSION} -c '{"Args":["CMT","CityhubToken","10000000"]}' -P 'OR ("enittMSP.member","scMSP.member")' >&log.txt
    #peer chaincode instantiate -o orderer1.energy.org:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n spcc -l ${LANGUAGE} -v ${VERSION} -c '{"Args":[]}' -P 'OR ("enittMSP.member","scMSP.member")' >&log2.txt

    peer chaincode instantiate -o orderer1.energy.org:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n tokencc -l ${LANGUAGE} -v ${VERSION} -c '{"Args":["EGT","EnergyToken","10000000"]}' -P 'OR ("ENITTMSP.member","ENITTMSP.admin")' >&log.txt
    peer chaincode instantiate -o orderer1.energy.org:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n spcc -l ${LANGUAGE} -v ${VERSION} -c '{"Args":[]}' >&log2.txt

    # peer chaincode instantiate -n tokencc -v ${VERSION} -c '{"Args":["CMT","energyMarketplaceToken","100000"]}' --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME  -l ${LANGUAGE}  
    # peer chaincode instantiate -o orderer1.energy.org:7050 --tls true --cafile /opt/gopath/src/energy.org/crypto/ordererOrganizations/energy.org/orderers/orderer1.energy.org/msp/tlscacerts/tlsca.energy.org-cert.pem -C marketplace-channel -n tokencc -l golang -v 1.0 -c '{"Args":["SWT","ShinWookToken","10000"]}' -P 'OR ("enittMSP.member","scMSP.member")' >&log.txt
    res=$?
    set +x

    echo "Wait 10 seconds for chaincode to be instantiated"
    sleep 10

    verifyResult $res "Chaincode instantiation on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' failed"

    echo "===================== Chaincode is instantiated on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' ===================== "
    echo
    
}

testChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}


    echo "===================== Chaincode is invoked on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' ===================== "
    echo    

    set -x
    peer chaincode invoke -n testcc -c '{"Args":["set", "a", "1000"]}' -C $CHANNEL_NAME -o orderer1.energy.org:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
    res=$?
    set +x

    verifyResult $res "Chaincode Invoke on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' failed"
    
    sleep 5

    echo "===================== Chaincode is queried on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' ===================== "
    echo   

    set -x
    peer chaincode query -n testcc -c '{"Args":["query","a"]}' -C $CHANNEL_NAME
    res=$?
    set +x

    verifyResult $res "Chaincode Qurey on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' failed"
    
}


## Create channel
echo "Creating channel..."
createChannel

## Join all the peers to the channel
echo "Having all peers join the channel..."
joinChannel

## Set the anchor peers for each org in the channel
echo "Updating anchor peers for enitt..."
updateAnchorPeers 1 enitt
echo "Updating anchor peers for sc..."
updateAnchorPeers 1 sc

echo "Building cityhub token chaincode..."
buildChaincode

## Install chaincode on peer1.enitt.energy.org and peer1.sc.energy.org
echo "Installing chaincode on peer1.enitt.energy.org"
installChaincode 1 enitt

# echo "Installing chaincode on peer1.enitt.energy.org"
# installChaincode 1 sc

# Instantiate chaincode on peer1.enitt
echo "Instantiating chaincode on peer1.enitt..."
instantiateChaincode 1 enitt

# Instantiate chaincode on peer1.enitt
# echo "Test chaincode on peer1.enitt..."
# testChaincode 1 enitt

# # Instantiate chaincode on peer1.enitt
# echo "Instantiating chaincode on peer1.enitt..."
# instantiateChaincode 0 2
